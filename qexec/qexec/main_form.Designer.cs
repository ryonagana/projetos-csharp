﻿namespace qexec
{
    partial class main_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gpBox = new System.Windows.Forms.GroupBox();
            this.btBrowseExec = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cpExecutable = new System.Windows.Forms.TextBox();
            this.gpMods = new System.Windows.Forms.GroupBox();
            this.cpOthers = new System.Windows.Forms.TextBox();
            this.rbOutros = new System.Windows.Forms.RadioButton();
            this.rbAction = new System.Windows.Forms.RadioButton();
            this.gpCfg = new System.Windows.Forms.GroupBox();
            this.cpCfgline = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lbLinhadeComando = new System.Windows.Forms.Label();
            this.cpLinhaCmd = new System.Windows.Forms.TextBox();
            this.btSaveAll = new System.Windows.Forms.Button();
            this.gpBox.SuspendLayout();
            this.gpMods.SuspendLayout();
            this.gpCfg.SuspendLayout();
            this.SuspendLayout();
            // 
            // gpBox
            // 
            this.gpBox.Controls.Add(this.btBrowseExec);
            this.gpBox.Controls.Add(this.label1);
            this.gpBox.Controls.Add(this.cpExecutable);
            this.gpBox.Location = new System.Drawing.Point(12, 12);
            this.gpBox.Name = "gpBox";
            this.gpBox.Size = new System.Drawing.Size(543, 56);
            this.gpBox.TabIndex = 0;
            this.gpBox.TabStop = false;
            this.gpBox.Text = "Details:";
            // 
            // btBrowseExec
            // 
            this.btBrowseExec.Location = new System.Drawing.Point(294, 19);
            this.btBrowseExec.Name = "btBrowseExec";
            this.btBrowseExec.Size = new System.Drawing.Size(50, 19);
            this.btBrowseExec.TabIndex = 2;
            this.btBrowseExec.Text = "Browse";
            this.btBrowseExec.UseVisualStyleBackColor = true;
            this.btBrowseExec.Click += new System.EventHandler(this.btBrowseExec_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Executable:";
            // 
            // cpExecutable
            // 
            this.cpExecutable.Location = new System.Drawing.Point(74, 19);
            this.cpExecutable.MaxLength = 255;
            this.cpExecutable.Name = "cpExecutable";
            this.cpExecutable.Size = new System.Drawing.Size(214, 20);
            this.cpExecutable.TabIndex = 0;
            // 
            // gpMods
            // 
            this.gpMods.Controls.Add(this.cpOthers);
            this.gpMods.Controls.Add(this.rbOutros);
            this.gpMods.Controls.Add(this.rbAction);
            this.gpMods.Location = new System.Drawing.Point(12, 74);
            this.gpMods.Name = "gpMods";
            this.gpMods.Size = new System.Drawing.Size(543, 65);
            this.gpMods.TabIndex = 1;
            this.gpMods.TabStop = false;
            this.gpMods.Text = "Mod:";
            // 
            // cpOthers
            // 
            this.cpOthers.Location = new System.Drawing.Point(111, 39);
            this.cpOthers.MaxLength = 32;
            this.cpOthers.Name = "cpOthers";
            this.cpOthers.Size = new System.Drawing.Size(152, 20);
            this.cpOthers.TabIndex = 2;
            this.cpOthers.TextChanged += new System.EventHandler(this.cpOthers_TextChanged);
            // 
            // rbOutros
            // 
            this.rbOutros.AutoSize = true;
            this.rbOutros.Location = new System.Drawing.Point(8, 39);
            this.rbOutros.Name = "rbOutros";
            this.rbOutros.Size = new System.Drawing.Size(56, 17);
            this.rbOutros.TabIndex = 1;
            this.rbOutros.TabStop = true;
            this.rbOutros.Text = "Others";
            this.rbOutros.UseVisualStyleBackColor = true;
            this.rbOutros.CheckedChanged += new System.EventHandler(this.rbOutros_CheckedChanged);
            // 
            // rbAction
            // 
            this.rbAction.AutoSize = true;
            this.rbAction.Location = new System.Drawing.Point(8, 20);
            this.rbAction.Name = "rbAction";
            this.rbAction.Size = new System.Drawing.Size(55, 17);
            this.rbAction.TabIndex = 0;
            this.rbAction.TabStop = true;
            this.rbAction.Text = "Action";
            this.rbAction.UseVisualStyleBackColor = true;
            this.rbAction.CheckedChanged += new System.EventHandler(this.rbAction_CheckedChanged);
            // 
            // gpCfg
            // 
            this.gpCfg.Controls.Add(this.cpCfgline);
            this.gpCfg.Controls.Add(this.label2);
            this.gpCfg.Location = new System.Drawing.Point(12, 145);
            this.gpCfg.Name = "gpCfg";
            this.gpCfg.Size = new System.Drawing.Size(543, 46);
            this.gpCfg.TabIndex = 2;
            this.gpCfg.TabStop = false;
            this.gpCfg.Text = "Config";
            // 
            // cpCfgline
            // 
            this.cpCfgline.Location = new System.Drawing.Point(111, 17);
            this.cpCfgline.Name = "cpCfgline";
            this.cpCfgline.Size = new System.Drawing.Size(177, 20);
            this.cpCfgline.TabIndex = 1;
            this.cpCfgline.TextChanged += new System.EventHandler(this.cpCfgline_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Config name (*.cfg)";
            // 
            // lbLinhadeComando
            // 
            this.lbLinhadeComando.AutoSize = true;
            this.lbLinhadeComando.Location = new System.Drawing.Point(12, 198);
            this.lbLinhadeComando.Name = "lbLinhadeComando";
            this.lbLinhadeComando.Size = new System.Drawing.Size(99, 13);
            this.lbLinhadeComando.TabIndex = 3;
            this.lbLinhadeComando.Text = "Linha de Comando:";
            // 
            // cpLinhaCmd
            // 
            this.cpLinhaCmd.Location = new System.Drawing.Point(123, 198);
            this.cpLinhaCmd.Name = "cpLinhaCmd";
            this.cpLinhaCmd.ReadOnly = true;
            this.cpLinhaCmd.Size = new System.Drawing.Size(295, 20);
            this.cpLinhaCmd.TabIndex = 4;
            // 
            // btSaveAll
            // 
            this.btSaveAll.Location = new System.Drawing.Point(460, 198);
            this.btSaveAll.Name = "btSaveAll";
            this.btSaveAll.Size = new System.Drawing.Size(95, 23);
            this.btSaveAll.TabIndex = 5;
            this.btSaveAll.Text = "Save and  Exit";
            this.btSaveAll.UseVisualStyleBackColor = true;
            this.btSaveAll.Click += new System.EventHandler(this.btSaveAll_Click);
            // 
            // main_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(567, 256);
            this.Controls.Add(this.btSaveAll);
            this.Controls.Add(this.cpLinhaCmd);
            this.Controls.Add(this.lbLinhadeComando);
            this.Controls.Add(this.gpCfg);
            this.Controls.Add(this.gpMods);
            this.Controls.Add(this.gpBox);
            this.Name = "main_form";
            this.ShowIcon = false;
            this.Text = "Quake CFG Exec";
            this.Load += new System.EventHandler(this.main_form_Load);
            this.gpBox.ResumeLayout(false);
            this.gpBox.PerformLayout();
            this.gpMods.ResumeLayout(false);
            this.gpMods.PerformLayout();
            this.gpCfg.ResumeLayout(false);
            this.gpCfg.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gpBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox cpExecutable;
        private System.Windows.Forms.Button btBrowseExec;
        private System.Windows.Forms.GroupBox gpMods;
        private System.Windows.Forms.TextBox cpOthers;
        private System.Windows.Forms.RadioButton rbOutros;
        private System.Windows.Forms.RadioButton rbAction;
        private System.Windows.Forms.GroupBox gpCfg;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox cpCfgline;
        private System.Windows.Forms.Label lbLinhadeComando;
        private System.Windows.Forms.TextBox cpLinhaCmd;
        private System.Windows.Forms.Button btSaveAll;
    }
}