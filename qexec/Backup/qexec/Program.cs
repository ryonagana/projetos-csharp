﻿using System;
using System.Diagnostics;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace qexec
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            

            if (args.Length > 0 )
            {
                if (!File.Exists(@"config.xml"))
                {
                    MessageBox.Show("Configuration File not found\n You need create a new file ", "Warning:", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    Application.Run(new main_form());
                }
                else
                {
                    classes.ReadFile ReadF = new qexec.classes.ReadFile(@"config.xml");

                    for (int i = 0; i < args.Length; i++)
                    {
                        ReadF.CmdArgs += args[i] + " ";


                    }

                    

                  //MessageBox.Show(ReadF._ServerData[0] +  ReadF._ServerData[1] + ReadF._ServerData[2] + " "+ ReadF.CmdArgs);
                    try
                    {
                        Process.Start(ReadF._ServerData[3] + "\\" + ReadF._ServerData[0], ReadF._ServerData[1] +" "+ ReadF.CmdArgs + ReadF._ServerData[2]);

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ReadF._ServerData[3] + ReadF._ServerData[0] + " Not Found");

                    }
                    finally
                    {
                        ReadF.UnloadConfig();

                    }

                    

                }

                
                

            }
            else
            {
                Application.Run(new main_form());
            }
            
        }
    }
}
