﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace qexec
{
    public partial class main_form : Form
    {
        OpenFileDialog arqExe = new OpenFileDialog();
        private string[] cmdLine = new string[5];

        public main_form()
        {
            InitializeComponent();
        }

        private void btBrowseExec_Click(object sender, EventArgs e)
        {

            arqExe.Filter = "Executables|*.exe";
            arqExe.Title = "Open Quake 2 Client:";


            if (arqExe.ShowDialog() == DialogResult.OK)
            {

                cpExecutable.Text = Path.GetFileName(arqExe.FileName.ToString());
                cmdLine[0] = cpExecutable.Text;
                cpLinhaCmd.Text = cmdLine[0] + cmdLine[1] + cmdLine[2] + cmdLine[3];
                cmdLine[4] = Path.GetDirectoryName(arqExe.FileName.ToString());



            }

        }

        private void rbAction_CheckedChanged(object sender, EventArgs e)
        {
            if (rbAction.Checked == true)
            {
                cpOthers.Enabled = false;
                cmdLine[1] = " +set game action";
                cpLinhaCmd.Text = cmdLine[0] + cmdLine[1] + cmdLine[2] + cmdLine[3];

            }
            else
            {
                cmdLine[1] = "";

            }
        }

        private void rbOutros_CheckedChanged(object sender, EventArgs e)
        {
            if (rbOutros.Checked == true)
            {
                cpOthers.Enabled = true;
                


            }
            
        }

        private void cpOthers_TextChanged(object sender, EventArgs e)
        {
            if (cpOthers.Text.Length.Equals(0))
            {

                cmdLine[1] = "";

            }
            else
            {

                cmdLine[1] = " +game " + cpOthers.Text;
                cpLinhaCmd.Text = cmdLine[0] + cmdLine[1] + cmdLine[2];

            }

        }

        private void cpCfgline_TextChanged(object sender, EventArgs e)
        {
            if (cpCfgline.Text.Length.Equals(0))
            {
                cmdLine[2] = "";

            }
            else
            {

                cmdLine[2] = " +exec " + cpCfgline.Text + ".cfg";
                cpLinhaCmd.Text = cmdLine[0] + cmdLine[1] + cmdLine[2];
          
            }

           
            

        }

        private void main_form_Load(object sender, EventArgs e)
        {

        }

        private void btSaveAll_Click(object sender, EventArgs e)
        {
            classes.SaveFile Salvar = new qexec.classes.SaveFile(@"config.xml");
            Salvar.AddElement("exepath", cmdLine[4]);
            Salvar.AddElement("executable", cmdLine[0]);
            Salvar.AddElement("mod", cmdLine[1]);
            Salvar.AddElement("cfgname", cmdLine[2]);
            Salvar.CloseSave();

            if (!Salvar.FileCreated() == true)
            {

                MessageBox.Show("File: " + Salvar._XMLName + " not Created", "=(", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);



            }
            else
            {

                MessageBox.Show("File: " + Salvar._XMLName + " Created", "=)", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);

            }

        }

    

 
    }
}
