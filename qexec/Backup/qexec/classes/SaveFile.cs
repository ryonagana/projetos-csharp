﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
namespace qexec.classes
{
    class SaveFile
    {
        private XmlTextWriter SaveDoc;
        public string _XMLName { set; get; }


        public SaveFile(string filename)
        {
            SaveDoc = new XmlTextWriter(filename, Encoding.UTF8);
            _XMLName = filename;
            SaveDoc.Formatting = Formatting.Indented;
            SaveDoc.WriteStartElement("config");


                       
        }

        public void AddElement(string elementName, string elementContent)
        {

            SaveDoc.WriteStartElement(elementName);
            SaveDoc.WriteString(elementContent);
            SaveDoc.WriteEndElement();

        }

        public void CloseSave()
        {
            SaveDoc.WriteEndElement();
            SaveDoc.Close();

        }

        public bool FileCreated()
        {
            if (!File.Exists(_XMLName))
            {
                return false;


            }
            else
            {
                return true;

            }

        }


    


    }
}
