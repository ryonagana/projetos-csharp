﻿using System;
using System.Xml;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace qexec.classes
{
    class ReadFile
    {

        public string[] _ServerData = new string[4];
        public string CmdArgs;

        private XmlDocument RDoc = new XmlDocument();
        

        public ReadFile(string filename)
        {
            XmlNodeList MainNode = RDoc.GetElementsByTagName("config");
         RDoc.Load(filename);

         foreach (XmlNode nodes in MainNode)
         {
             XmlElement Element = (XmlElement)nodes;

             _ServerData[0] = Element.GetElementsByTagName("executable")[0].InnerText;
             _ServerData[1] = Element.GetElementsByTagName("mod")[0].InnerText;
             _ServerData[2] = Element.GetElementsByTagName("cfgname")[0].InnerText;
             _ServerData[3] = Element.GetElementsByTagName("exepath")[0].InnerText;

         }





        }

        public void UnloadConfig()
        {
            RDoc.RemoveAll();
        }

    }
}
