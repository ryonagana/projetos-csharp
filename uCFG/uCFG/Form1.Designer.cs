﻿namespace uCFG
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.btnSalvar = new System.Windows.Forms.Button();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.abaMovimentacao = new System.Windows.Forms.TabPage();
            this.scbRun = new Quake2.SetCheckBox();
            this.kbbMoveUp = new Quake2.KeyBindBox();
            this.lblMoveUp = new System.Windows.Forms.Label();
            this.kbbMoveRight = new Quake2.KeyBindBox();
            this.lblMoveRight = new System.Windows.Forms.Label();
            this.kbbBack = new Quake2.KeyBindBox();
            this.lblBack = new System.Windows.Forms.Label();
            this.kbbMoveLeft = new Quake2.KeyBindBox();
            this.kbbForward = new Quake2.KeyBindBox();
            this.lblLeft = new System.Windows.Forms.Label();
            this.lblForward = new System.Windows.Forms.Label();
            this.abaArmas = new System.Windows.Forms.TabPage();
            this.scbMAccel = new Quake2.SetCheckBox();
            this.scbXpFix = new Quake2.SetCheckBox();
            this.lblSensitivity = new System.Windows.Forms.Label();
            this.lblReload = new System.Windows.Forms.Label();
            this.lblAttack = new System.Windows.Forms.Label();
            this.svSensitivity = new Quake2.SetValue();
            this.kbbReload = new Quake2.KeyBindBox();
            this.kbbAtirar = new Quake2.KeyBindBox();
            this.abaAction = new System.Windows.Forms.TabPage();
            this.kbbWeapon = new Quake2.KeyBindBox();
            this.lblWeapon = new System.Windows.Forms.Label();
            this.lblBandAid = new System.Windows.Forms.Label();
            this.kbbBandAid = new Quake2.KeyBindBox();
            this.abaVideo = new System.Windows.Forms.TabPage();
            this.lblShadows = new System.Windows.Forms.Label();
            this.lblConTrans = new System.Windows.Forms.Label();
            this.lblCelShadingWidth = new System.Windows.Forms.Label();
            this.lblResolution = new System.Windows.Forms.Label();
            this.lblModulate = new System.Windows.Forms.Label();
            this.soShadows = new Quake2.SetOption();
            this.svConTrans = new Quake2.SetValue();
            this.svCelShadingWidth = new Quake2.SetValue();
            this.scbCelShading = new Quake2.SetCheckBox();
            this.soResolution = new Quake2.SetOption();
            this.svModulate = new Quake2.SetValue();
            this.scbLuzColorida = new Quake2.SetCheckBox();
            this.abaVideo2 = new System.Windows.Forms.TabPage();
            this.svWaterWaves = new Quake2.SetValue();
            this.lblWaterWaves = new System.Windows.Forms.Label();
            this.gbDisplay = new System.Windows.Forms.GroupBox();
            this.scbDrawChatHud = new Quake2.SetCheckBox();
            this.scbDrawFps = new Quake2.SetCheckBox();
            this.gbReplace = new System.Windows.Forms.GroupBox();
            this.scbReplacePcx = new Quake2.SetCheckBox();
            this.scbReplaceWal = new Quake2.SetCheckBox();
            this.scbReplaceMd2 = new Quake2.SetCheckBox();
            this.lblFogDensity = new System.Windows.Forms.Label();
            this.scbStainMaps = new Quake2.SetCheckBox();
            this.svFogDensity = new Quake2.SetValue();
            this.scbFog = new Quake2.SetCheckBox();
            this.btnCarregar = new System.Windows.Forms.Button();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.tabControl.SuspendLayout();
            this.abaMovimentacao.SuspendLayout();
            this.abaArmas.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.svSensitivity)).BeginInit();
            this.abaAction.SuspendLayout();
            this.abaVideo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.svConTrans)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.svCelShadingWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.svModulate)).BeginInit();
            this.abaVideo2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.svWaterWaves)).BeginInit();
            this.gbDisplay.SuspendLayout();
            this.gbReplace.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.svFogDensity)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSalvar
            // 
            this.btnSalvar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSalvar.Location = new System.Drawing.Point(12, 254);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(75, 23);
            this.btnSalvar.TabIndex = 2;
            this.btnSalvar.Text = "Salvar";
            this.btnSalvar.UseVisualStyleBackColor = true;
            this.btnSalvar.Click += new System.EventHandler(this.btnSalvar_Click);
            // 
            // tabControl
            // 
            this.tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl.Controls.Add(this.abaMovimentacao);
            this.tabControl.Controls.Add(this.abaArmas);
            this.tabControl.Controls.Add(this.abaAction);
            this.tabControl.Controls.Add(this.abaVideo);
            this.tabControl.Controls.Add(this.abaVideo2);
            this.tabControl.Location = new System.Drawing.Point(12, 12);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(227, 236);
            this.tabControl.TabIndex = 3;
            // 
            // abaMovimentacao
            // 
            this.abaMovimentacao.Controls.Add(this.scbRun);
            this.abaMovimentacao.Controls.Add(this.kbbMoveUp);
            this.abaMovimentacao.Controls.Add(this.lblMoveUp);
            this.abaMovimentacao.Controls.Add(this.kbbMoveRight);
            this.abaMovimentacao.Controls.Add(this.lblMoveRight);
            this.abaMovimentacao.Controls.Add(this.kbbBack);
            this.abaMovimentacao.Controls.Add(this.lblBack);
            this.abaMovimentacao.Controls.Add(this.kbbMoveLeft);
            this.abaMovimentacao.Controls.Add(this.kbbForward);
            this.abaMovimentacao.Controls.Add(this.lblLeft);
            this.abaMovimentacao.Controls.Add(this.lblForward);
            this.abaMovimentacao.Location = new System.Drawing.Point(4, 22);
            this.abaMovimentacao.Name = "abaMovimentacao";
            this.abaMovimentacao.Padding = new System.Windows.Forms.Padding(3);
            this.abaMovimentacao.Size = new System.Drawing.Size(219, 210);
            this.abaMovimentacao.TabIndex = 0;
            this.abaMovimentacao.Text = "Movimentação";
            this.abaMovimentacao.UseVisualStyleBackColor = true;
            // 
            // scbRun
            // 
            this.scbRun.AutoSize = true;
            this.scbRun.Checked = true;
            this.scbRun.CheckState = System.Windows.Forms.CheckState.Checked;
            this.scbRun.Location = new System.Drawing.Point(6, 152);
            this.scbRun.Name = "scbRun";
            this.scbRun.SetCommand = null;
            this.scbRun.Size = new System.Drawing.Size(93, 17);
            this.scbRun.TabIndex = 11;
            this.scbRun.Text = "Sempre Correr";
            this.scbRun.UseVisualStyleBackColor = true;
            // 
            // kbbMoveUp
            // 
            this.kbbMoveUp.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.kbbMoveUp.BindAction = "";
            this.kbbMoveUp.DetectKeys = true;
            this.kbbMoveUp.Location = new System.Drawing.Point(117, 110);
            this.kbbMoveUp.Name = "kbbMoveUp";
            this.kbbMoveUp.Size = new System.Drawing.Size(92, 20);
            this.kbbMoveUp.TabIndex = 10;
            // 
            // lblMoveUp
            // 
            this.lblMoveUp.AutoSize = true;
            this.lblMoveUp.Location = new System.Drawing.Point(6, 113);
            this.lblMoveUp.Name = "lblMoveUp";
            this.lblMoveUp.Size = new System.Drawing.Size(31, 13);
            this.lblMoveUp.TabIndex = 9;
            this.lblMoveUp.Text = "Pular";
            // 
            // kbbMoveRight
            // 
            this.kbbMoveRight.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.kbbMoveRight.BindAction = "";
            this.kbbMoveRight.DetectKeys = true;
            this.kbbMoveRight.Location = new System.Drawing.Point(117, 84);
            this.kbbMoveRight.Name = "kbbMoveRight";
            this.kbbMoveRight.Size = new System.Drawing.Size(92, 20);
            this.kbbMoveRight.TabIndex = 8;
            // 
            // lblMoveRight
            // 
            this.lblMoveRight.AutoSize = true;
            this.lblMoveRight.Location = new System.Drawing.Point(6, 87);
            this.lblMoveRight.Name = "lblMoveRight";
            this.lblMoveRight.Size = new System.Drawing.Size(86, 13);
            this.lblMoveRight.TabIndex = 7;
            this.lblMoveRight.Text = "Andar pra Direita";
            // 
            // kbbBack
            // 
            this.kbbBack.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.kbbBack.BindAction = "";
            this.kbbBack.DetectKeys = true;
            this.kbbBack.Location = new System.Drawing.Point(117, 58);
            this.kbbBack.Name = "kbbBack";
            this.kbbBack.Size = new System.Drawing.Size(92, 20);
            this.kbbBack.TabIndex = 6;
            // 
            // lblBack
            // 
            this.lblBack.AutoSize = true;
            this.lblBack.Location = new System.Drawing.Point(6, 61);
            this.lblBack.Name = "lblBack";
            this.lblBack.Size = new System.Drawing.Size(77, 13);
            this.lblBack.TabIndex = 5;
            this.lblBack.Text = "Andar pra Tras";
            // 
            // kbbMoveLeft
            // 
            this.kbbMoveLeft.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.kbbMoveLeft.BindAction = "";
            this.kbbMoveLeft.DetectKeys = true;
            this.kbbMoveLeft.Location = new System.Drawing.Point(117, 32);
            this.kbbMoveLeft.Name = "kbbMoveLeft";
            this.kbbMoveLeft.Size = new System.Drawing.Size(92, 20);
            this.kbbMoveLeft.TabIndex = 4;
            // 
            // kbbForward
            // 
            this.kbbForward.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.kbbForward.BindAction = "";
            this.kbbForward.DetectKeys = true;
            this.kbbForward.Location = new System.Drawing.Point(117, 6);
            this.kbbForward.Name = "kbbForward";
            this.kbbForward.Size = new System.Drawing.Size(92, 20);
            this.kbbForward.TabIndex = 3;
            // 
            // lblLeft
            // 
            this.lblLeft.AutoSize = true;
            this.lblLeft.Location = new System.Drawing.Point(6, 35);
            this.lblLeft.Name = "lblLeft";
            this.lblLeft.Size = new System.Drawing.Size(101, 13);
            this.lblLeft.TabIndex = 2;
            this.lblLeft.Text = "Andar pra Esquerda";
            // 
            // lblForward
            // 
            this.lblForward.AutoSize = true;
            this.lblForward.Location = new System.Drawing.Point(6, 9);
            this.lblForward.Name = "lblForward";
            this.lblForward.Size = new System.Drawing.Size(86, 13);
            this.lblForward.TabIndex = 1;
            this.lblForward.Text = "Andar pra Frente";
            // 
            // abaArmas
            // 
            this.abaArmas.Controls.Add(this.scbMAccel);
            this.abaArmas.Controls.Add(this.scbXpFix);
            this.abaArmas.Controls.Add(this.lblSensitivity);
            this.abaArmas.Controls.Add(this.lblReload);
            this.abaArmas.Controls.Add(this.lblAttack);
            this.abaArmas.Controls.Add(this.svSensitivity);
            this.abaArmas.Controls.Add(this.kbbReload);
            this.abaArmas.Controls.Add(this.kbbAtirar);
            this.abaArmas.Location = new System.Drawing.Point(4, 22);
            this.abaArmas.Name = "abaArmas";
            this.abaArmas.Padding = new System.Windows.Forms.Padding(3);
            this.abaArmas.Size = new System.Drawing.Size(219, 210);
            this.abaArmas.TabIndex = 1;
            this.abaArmas.Text = "Armas";
            this.abaArmas.UseVisualStyleBackColor = true;
            // 
            // scbMAccel
            // 
            this.scbMAccel.AutoSize = true;
            this.scbMAccel.Location = new System.Drawing.Point(9, 107);
            this.scbMAccel.Name = "scbMAccel";
            this.scbMAccel.SetCommand = null;
            this.scbMAccel.Size = new System.Drawing.Size(115, 17);
            this.scbMAccel.TabIndex = 13;
            this.scbMAccel.Text = "Acelerar do Mouse";
            this.scbMAccel.UseVisualStyleBackColor = true;
            // 
            // scbXpFix
            // 
            this.scbXpFix.AutoSize = true;
            this.scbXpFix.Location = new System.Drawing.Point(9, 84);
            this.scbXpFix.Name = "scbXpFix";
            this.scbXpFix.SetCommand = null;
            this.scbXpFix.Size = new System.Drawing.Size(182, 17);
            this.scbXpFix.TabIndex = 12;
            this.scbXpFix.Text = "Desabilitar aceleração do WinXP";
            this.scbXpFix.UseVisualStyleBackColor = true;
            // 
            // lblSensitivity
            // 
            this.lblSensitivity.AutoSize = true;
            this.lblSensitivity.Location = new System.Drawing.Point(6, 60);
            this.lblSensitivity.Name = "lblSensitivity";
            this.lblSensitivity.Size = new System.Drawing.Size(54, 13);
            this.lblSensitivity.TabIndex = 11;
            this.lblSensitivity.Text = "Sensitivity";
            // 
            // lblReload
            // 
            this.lblReload.AutoSize = true;
            this.lblReload.Location = new System.Drawing.Point(6, 35);
            this.lblReload.Name = "lblReload";
            this.lblReload.Size = new System.Drawing.Size(60, 13);
            this.lblReload.TabIndex = 6;
            this.lblReload.Text = "Recarregar";
            // 
            // lblAttack
            // 
            this.lblAttack.AutoSize = true;
            this.lblAttack.Location = new System.Drawing.Point(6, 9);
            this.lblAttack.Name = "lblAttack";
            this.lblAttack.Size = new System.Drawing.Size(31, 13);
            this.lblAttack.TabIndex = 4;
            this.lblAttack.Text = "Atirar";
            // 
            // svSensitivity
            // 
            this.svSensitivity.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.svSensitivity.DecimalPlaces = 1;
            this.svSensitivity.Location = new System.Drawing.Point(81, 58);
            this.svSensitivity.Name = "svSensitivity";
            this.svSensitivity.SetCommand = "";
            this.svSensitivity.Size = new System.Drawing.Size(128, 20);
            this.svSensitivity.TabIndex = 10;
            // 
            // kbbReload
            // 
            this.kbbReload.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.kbbReload.BindAction = "";
            this.kbbReload.DetectKeys = true;
            this.kbbReload.Location = new System.Drawing.Point(81, 32);
            this.kbbReload.Name = "kbbReload";
            this.kbbReload.Size = new System.Drawing.Size(128, 20);
            this.kbbReload.TabIndex = 7;
            // 
            // kbbAtirar
            // 
            this.kbbAtirar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.kbbAtirar.BindAction = "";
            this.kbbAtirar.DetectKeys = true;
            this.kbbAtirar.Location = new System.Drawing.Point(81, 6);
            this.kbbAtirar.Name = "kbbAtirar";
            this.kbbAtirar.Size = new System.Drawing.Size(128, 20);
            this.kbbAtirar.TabIndex = 5;
            // 
            // abaAction
            // 
            this.abaAction.Controls.Add(this.kbbWeapon);
            this.abaAction.Controls.Add(this.lblWeapon);
            this.abaAction.Controls.Add(this.lblBandAid);
            this.abaAction.Controls.Add(this.kbbBandAid);
            this.abaAction.Location = new System.Drawing.Point(4, 22);
            this.abaAction.Name = "abaAction";
            this.abaAction.Padding = new System.Windows.Forms.Padding(3);
            this.abaAction.Size = new System.Drawing.Size(219, 210);
            this.abaAction.TabIndex = 3;
            this.abaAction.Text = "Action";
            this.abaAction.UseVisualStyleBackColor = true;
            // 
            // kbbWeapon
            // 
            this.kbbWeapon.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.kbbWeapon.BindAction = "";
            this.kbbWeapon.DetectKeys = true;
            this.kbbWeapon.Location = new System.Drawing.Point(131, 32);
            this.kbbWeapon.Name = "kbbWeapon";
            this.kbbWeapon.Size = new System.Drawing.Size(78, 20);
            this.kbbWeapon.TabIndex = 12;
            // 
            // lblWeapon
            // 
            this.lblWeapon.AutoSize = true;
            this.lblWeapon.Location = new System.Drawing.Point(6, 35);
            this.lblWeapon.Name = "lblWeapon";
            this.lblWeapon.Size = new System.Drawing.Size(119, 13);
            this.lblWeapon.TabIndex = 11;
            this.lblWeapon.Text = "Trocar Funcao da Arma";
            // 
            // lblBandAid
            // 
            this.lblBandAid.AutoSize = true;
            this.lblBandAid.Location = new System.Drawing.Point(6, 9);
            this.lblBandAid.Name = "lblBandAid";
            this.lblBandAid.Size = new System.Drawing.Size(50, 13);
            this.lblBandAid.TabIndex = 10;
            this.lblBandAid.Text = "Bandage";
            // 
            // kbbBandAid
            // 
            this.kbbBandAid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.kbbBandAid.BindAction = "";
            this.kbbBandAid.DetectKeys = true;
            this.kbbBandAid.Location = new System.Drawing.Point(131, 6);
            this.kbbBandAid.Name = "kbbBandAid";
            this.kbbBandAid.Size = new System.Drawing.Size(78, 20);
            this.kbbBandAid.TabIndex = 9;
            // 
            // abaVideo
            // 
            this.abaVideo.Controls.Add(this.lblShadows);
            this.abaVideo.Controls.Add(this.lblConTrans);
            this.abaVideo.Controls.Add(this.lblCelShadingWidth);
            this.abaVideo.Controls.Add(this.lblResolution);
            this.abaVideo.Controls.Add(this.lblModulate);
            this.abaVideo.Controls.Add(this.soShadows);
            this.abaVideo.Controls.Add(this.svConTrans);
            this.abaVideo.Controls.Add(this.svCelShadingWidth);
            this.abaVideo.Controls.Add(this.scbCelShading);
            this.abaVideo.Controls.Add(this.soResolution);
            this.abaVideo.Controls.Add(this.svModulate);
            this.abaVideo.Controls.Add(this.scbLuzColorida);
            this.abaVideo.Location = new System.Drawing.Point(4, 22);
            this.abaVideo.Name = "abaVideo";
            this.abaVideo.Padding = new System.Windows.Forms.Padding(3);
            this.abaVideo.Size = new System.Drawing.Size(219, 210);
            this.abaVideo.TabIndex = 2;
            this.abaVideo.Text = "Video";
            this.abaVideo.UseVisualStyleBackColor = true;
            // 
            // lblShadows
            // 
            this.lblShadows.AutoSize = true;
            this.lblShadows.Location = new System.Drawing.Point(6, 116);
            this.lblShadows.Name = "lblShadows";
            this.lblShadows.Size = new System.Drawing.Size(48, 13);
            this.lblShadows.TabIndex = 11;
            this.lblShadows.Text = "Sombras";
            // 
            // lblConTrans
            // 
            this.lblConTrans.AutoSize = true;
            this.lblConTrans.Location = new System.Drawing.Point(6, 89);
            this.lblConTrans.Name = "lblConTrans";
            this.lblConTrans.Size = new System.Drawing.Size(131, 13);
            this.lblConTrans.TabIndex = 9;
            this.lblConTrans.Text = "Transparência do Console";
            // 
            // lblCelShadingWidth
            // 
            this.lblCelShadingWidth.AutoSize = true;
            this.lblCelShadingWidth.Enabled = false;
            this.lblCelShadingWidth.Location = new System.Drawing.Point(77, 54);
            this.lblCelShadingWidth.Name = "lblCelShadingWidth";
            this.lblCelShadingWidth.Size = new System.Drawing.Size(100, 13);
            this.lblCelShadingWidth.TabIndex = 7;
            this.lblCelShadingWidth.Text = "Espessura da Linha";
            // 
            // lblResolution
            // 
            this.lblResolution.AutoSize = true;
            this.lblResolution.Location = new System.Drawing.Point(6, 183);
            this.lblResolution.Name = "lblResolution";
            this.lblResolution.Size = new System.Drawing.Size(58, 13);
            this.lblResolution.TabIndex = 4;
            this.lblResolution.Text = "Resolução";
            // 
            // lblModulate
            // 
            this.lblModulate.AutoSize = true;
            this.lblModulate.Location = new System.Drawing.Point(6, 156);
            this.lblModulate.Name = "lblModulate";
            this.lblModulate.Size = new System.Drawing.Size(51, 13);
            this.lblModulate.TabIndex = 2;
            this.lblModulate.Text = "Modulate";
            // 
            // soShadows
            // 
            this.soShadows.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.soShadows.FormattingEnabled = true;
            this.soShadows.Items.AddRange(new object[] {
            "Desativadas",
            "Baixa Qualidade",
            "Alta Qualidade"});
            this.soShadows.Location = new System.Drawing.Point(76, 113);
            this.soShadows.Name = "soShadows";
            this.soShadows.SetCommand = "";
            this.soShadows.Size = new System.Drawing.Size(133, 21);
            this.soShadows.TabIndex = 10;
            // 
            // svConTrans
            // 
            this.svConTrans.DecimalPlaces = 1;
            this.svConTrans.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.svConTrans.Location = new System.Drawing.Point(143, 87);
            this.svConTrans.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.svConTrans.Name = "svConTrans";
            this.svConTrans.SetCommand = "";
            this.svConTrans.Size = new System.Drawing.Size(50, 20);
            this.svConTrans.TabIndex = 8;
            // 
            // svCelShadingWidth
            // 
            this.svCelShadingWidth.Enabled = false;
            this.svCelShadingWidth.Location = new System.Drawing.Point(21, 52);
            this.svCelShadingWidth.Name = "svCelShadingWidth";
            this.svCelShadingWidth.SetCommand = "";
            this.svCelShadingWidth.Size = new System.Drawing.Size(50, 20);
            this.svCelShadingWidth.TabIndex = 6;
            this.svCelShadingWidth.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            // 
            // scbCelShading
            // 
            this.scbCelShading.AutoSize = true;
            this.scbCelShading.Location = new System.Drawing.Point(9, 29);
            this.scbCelShading.Name = "scbCelShading";
            this.scbCelShading.SetCommand = "";
            this.scbCelShading.Size = new System.Drawing.Size(186, 17);
            this.scbCelShading.TabIndex = 5;
            this.scbCelShading.Text = "Objetos Contornados (Celshading)";
            this.scbCelShading.UseVisualStyleBackColor = true;
            this.scbCelShading.CheckedChanged += new System.EventHandler(this.scbCelShading_CheckedChanged);
            // 
            // soResolution
            // 
            this.soResolution.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.soResolution.FormattingEnabled = true;
            this.soResolution.Items.AddRange(new object[] {
            "320x240",
            "400x300",
            "512x384",
            "640x480",
            "800x600",
            "960x720",
            "1024x768",
            "1152x864",
            "1280x960",
            "1600x1200",
            "2048x1536",
            "1024x480",
            "1280x768",
            "1280x1024"});
            this.soResolution.Location = new System.Drawing.Point(76, 180);
            this.soResolution.Name = "soResolution";
            this.soResolution.SetCommand = "";
            this.soResolution.Size = new System.Drawing.Size(133, 21);
            this.soResolution.TabIndex = 3;
            // 
            // svModulate
            // 
            this.svModulate.DecimalPlaces = 1;
            this.svModulate.Location = new System.Drawing.Point(76, 154);
            this.svModulate.Name = "svModulate";
            this.svModulate.SetCommand = "";
            this.svModulate.Size = new System.Drawing.Size(50, 20);
            this.svModulate.TabIndex = 1;
            // 
            // scbLuzColorida
            // 
            this.scbLuzColorida.AutoSize = true;
            this.scbLuzColorida.Checked = true;
            this.scbLuzColorida.CheckState = System.Windows.Forms.CheckState.Checked;
            this.scbLuzColorida.Location = new System.Drawing.Point(9, 6);
            this.scbLuzColorida.Name = "scbLuzColorida";
            this.scbLuzColorida.SetCommand = "";
            this.scbLuzColorida.Size = new System.Drawing.Size(100, 17);
            this.scbLuzColorida.TabIndex = 0;
            this.scbLuzColorida.Text = "Luzes Coloridas";
            this.scbLuzColorida.UseVisualStyleBackColor = true;
            // 
            // abaVideo2
            // 
            this.abaVideo2.Controls.Add(this.svWaterWaves);
            this.abaVideo2.Controls.Add(this.lblWaterWaves);
            this.abaVideo2.Controls.Add(this.gbDisplay);
            this.abaVideo2.Controls.Add(this.gbReplace);
            this.abaVideo2.Controls.Add(this.lblFogDensity);
            this.abaVideo2.Controls.Add(this.scbStainMaps);
            this.abaVideo2.Controls.Add(this.svFogDensity);
            this.abaVideo2.Controls.Add(this.scbFog);
            this.abaVideo2.Location = new System.Drawing.Point(4, 22);
            this.abaVideo2.Name = "abaVideo2";
            this.abaVideo2.Padding = new System.Windows.Forms.Padding(3);
            this.abaVideo2.Size = new System.Drawing.Size(219, 210);
            this.abaVideo2.TabIndex = 4;
            this.abaVideo2.Text = "Video 2";
            this.abaVideo2.UseVisualStyleBackColor = true;
            // 
            // svWaterWaves
            // 
            this.svWaterWaves.Location = new System.Drawing.Point(137, 180);
            this.svWaterWaves.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.svWaterWaves.Name = "svWaterWaves";
            this.svWaterWaves.SetCommand = "";
            this.svWaterWaves.Size = new System.Drawing.Size(50, 20);
            this.svWaterWaves.TabIndex = 17;
            // 
            // lblWaterWaves
            // 
            this.lblWaterWaves.AutoSize = true;
            this.lblWaterWaves.Location = new System.Drawing.Point(6, 182);
            this.lblWaterWaves.Name = "lblWaterWaves";
            this.lblWaterWaves.Size = new System.Drawing.Size(125, 13);
            this.lblWaterWaves.TabIndex = 16;
            this.lblWaterWaves.Text = "Altura de Ondas na agua";
            // 
            // gbDisplay
            // 
            this.gbDisplay.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gbDisplay.Controls.Add(this.scbDrawChatHud);
            this.gbDisplay.Controls.Add(this.scbDrawFps);
            this.gbDisplay.Location = new System.Drawing.Point(6, 106);
            this.gbDisplay.Name = "gbDisplay";
            this.gbDisplay.Size = new System.Drawing.Size(207, 45);
            this.gbDisplay.TabIndex = 13;
            this.gbDisplay.TabStop = false;
            this.gbDisplay.Text = "Display";
            // 
            // scbDrawChatHud
            // 
            this.scbDrawChatHud.AutoSize = true;
            this.scbDrawChatHud.Location = new System.Drawing.Point(78, 19);
            this.scbDrawChatHud.Name = "scbDrawChatHud";
            this.scbDrawChatHud.SetCommand = "";
            this.scbDrawChatHud.Size = new System.Drawing.Size(71, 17);
            this.scbDrawChatHud.TabIndex = 11;
            this.scbDrawChatHud.Text = "Chat Hud";
            this.scbDrawChatHud.UseVisualStyleBackColor = true;
            // 
            // scbDrawFps
            // 
            this.scbDrawFps.AutoSize = true;
            this.scbDrawFps.Location = new System.Drawing.Point(6, 19);
            this.scbDrawFps.Name = "scbDrawFps";
            this.scbDrawFps.SetCommand = "";
            this.scbDrawFps.Size = new System.Drawing.Size(46, 17);
            this.scbDrawFps.TabIndex = 10;
            this.scbDrawFps.Text = "FPS";
            this.scbDrawFps.UseVisualStyleBackColor = true;
            // 
            // gbReplace
            // 
            this.gbReplace.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gbReplace.Controls.Add(this.scbReplacePcx);
            this.gbReplace.Controls.Add(this.scbReplaceWal);
            this.gbReplace.Controls.Add(this.scbReplaceMd2);
            this.gbReplace.Location = new System.Drawing.Point(6, 55);
            this.gbReplace.Name = "gbReplace";
            this.gbReplace.Size = new System.Drawing.Size(207, 45);
            this.gbReplace.TabIndex = 9;
            this.gbReplace.TabStop = false;
            this.gbReplace.Text = "Alta Qualidade";
            // 
            // scbReplacePcx
            // 
            this.scbReplacePcx.AutoSize = true;
            this.scbReplacePcx.Checked = true;
            this.scbReplacePcx.CheckState = System.Windows.Forms.CheckState.Checked;
            this.scbReplacePcx.Location = new System.Drawing.Point(146, 19);
            this.scbReplacePcx.Name = "scbReplacePcx";
            this.scbReplacePcx.SetCommand = "";
            this.scbReplacePcx.Size = new System.Drawing.Size(47, 17);
            this.scbReplacePcx.TabIndex = 12;
            this.scbReplacePcx.Text = "PCX";
            this.scbReplacePcx.UseVisualStyleBackColor = true;
            // 
            // scbReplaceWal
            // 
            this.scbReplaceWal.AutoSize = true;
            this.scbReplaceWal.Checked = true;
            this.scbReplaceWal.CheckState = System.Windows.Forms.CheckState.Checked;
            this.scbReplaceWal.Location = new System.Drawing.Point(78, 19);
            this.scbReplaceWal.Name = "scbReplaceWal";
            this.scbReplaceWal.SetCommand = "";
            this.scbReplaceWal.Size = new System.Drawing.Size(67, 17);
            this.scbReplaceWal.TabIndex = 11;
            this.scbReplaceWal.Text = "Texturas";
            this.scbReplaceWal.UseVisualStyleBackColor = true;
            // 
            // scbReplaceMd2
            // 
            this.scbReplaceMd2.AutoSize = true;
            this.scbReplaceMd2.Checked = true;
            this.scbReplaceMd2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.scbReplaceMd2.Location = new System.Drawing.Point(6, 19);
            this.scbReplaceMd2.Name = "scbReplaceMd2";
            this.scbReplaceMd2.SetCommand = "";
            this.scbReplaceMd2.Size = new System.Drawing.Size(66, 17);
            this.scbReplaceMd2.TabIndex = 10;
            this.scbReplaceMd2.Text = "Modelos";
            this.scbReplaceMd2.UseVisualStyleBackColor = true;
            // 
            // lblFogDensity
            // 
            this.lblFogDensity.AutoSize = true;
            this.lblFogDensity.Enabled = false;
            this.lblFogDensity.Location = new System.Drawing.Point(77, 31);
            this.lblFogDensity.Name = "lblFogDensity";
            this.lblFogDensity.Size = new System.Drawing.Size(114, 13);
            this.lblFogDensity.TabIndex = 8;
            this.lblFogDensity.Text = "Quantidade de neblina";
            // 
            // scbStainMaps
            // 
            this.scbStainMaps.AutoSize = true;
            this.scbStainMaps.Checked = true;
            this.scbStainMaps.CheckState = System.Windows.Forms.CheckState.Checked;
            this.scbStainMaps.Location = new System.Drawing.Point(6, 157);
            this.scbStainMaps.Name = "scbStainMaps";
            this.scbStainMaps.SetCommand = "";
            this.scbStainMaps.Size = new System.Drawing.Size(125, 17);
            this.scbStainMaps.TabIndex = 13;
            this.scbStainMaps.Text = "Manchas de Sangue";
            this.scbStainMaps.UseVisualStyleBackColor = true;
            // 
            // svFogDensity
            // 
            this.svFogDensity.DecimalPlaces = 1;
            this.svFogDensity.Enabled = false;
            this.svFogDensity.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.svFogDensity.Location = new System.Drawing.Point(21, 29);
            this.svFogDensity.Maximum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.svFogDensity.Name = "svFogDensity";
            this.svFogDensity.SetCommand = "";
            this.svFogDensity.Size = new System.Drawing.Size(50, 20);
            this.svFogDensity.TabIndex = 7;
            // 
            // scbFog
            // 
            this.scbFog.AutoSize = true;
            this.scbFog.Location = new System.Drawing.Point(6, 6);
            this.scbFog.Name = "scbFog";
            this.scbFog.SetCommand = "";
            this.scbFog.Size = new System.Drawing.Size(62, 17);
            this.scbFog.TabIndex = 0;
            this.scbFog.Text = "Neblina";
            this.scbFog.UseVisualStyleBackColor = true;
            this.scbFog.CheckedChanged += new System.EventHandler(this.scbFog_CheckedChanged);
            // 
            // btnCarregar
            // 
            this.btnCarregar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCarregar.Location = new System.Drawing.Point(93, 254);
            this.btnCarregar.Name = "btnCarregar";
            this.btnCarregar.Size = new System.Drawing.Size(75, 23);
            this.btnCarregar.TabIndex = 4;
            this.btnCarregar.Text = "Carregar";
            this.btnCarregar.UseVisualStyleBackColor = true;
            this.btnCarregar.Click += new System.EventHandler(this.btnCarregar_Click);
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.Title = "Salvar Config";
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog1";
            this.openFileDialog.Title = "Abrir Config";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(251, 289);
            this.Controls.Add(this.btnCarregar);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.btnSalvar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "uCFG v0.2";
            this.tabControl.ResumeLayout(false);
            this.abaMovimentacao.ResumeLayout(false);
            this.abaMovimentacao.PerformLayout();
            this.abaArmas.ResumeLayout(false);
            this.abaArmas.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.svSensitivity)).EndInit();
            this.abaAction.ResumeLayout(false);
            this.abaAction.PerformLayout();
            this.abaVideo.ResumeLayout(false);
            this.abaVideo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.svConTrans)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.svCelShadingWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.svModulate)).EndInit();
            this.abaVideo2.ResumeLayout(false);
            this.abaVideo2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.svWaterWaves)).EndInit();
            this.gbDisplay.ResumeLayout(false);
            this.gbDisplay.PerformLayout();
            this.gbReplace.ResumeLayout(false);
            this.gbReplace.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.svFogDensity)).EndInit();
            this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Button btnSalvar;
		private System.Windows.Forms.TabControl tabControl;
		private System.Windows.Forms.TabPage abaMovimentacao;
		private System.Windows.Forms.TabPage abaArmas;
		private System.Windows.Forms.Label lblLeft;
		private System.Windows.Forms.Label lblForward;
		private Quake2.KeyBindBox kbbMoveLeft;
		private Quake2.KeyBindBox kbbForward;
		private Quake2.KeyBindBox kbbBack;
		private System.Windows.Forms.Label lblBack;
		private Quake2.KeyBindBox kbbMoveRight;
		private System.Windows.Forms.Label lblMoveRight;
		private Quake2.KeyBindBox kbbMoveUp;
		private System.Windows.Forms.Label lblMoveUp;
		private Quake2.KeyBindBox kbbAtirar;
		private System.Windows.Forms.Label lblAttack;
		private Quake2.KeyBindBox kbbReload;
		private System.Windows.Forms.Label lblReload;
		private System.Windows.Forms.Button btnCarregar;
		private System.Windows.Forms.TabPage abaVideo;
		private Quake2.SetCheckBox scbLuzColorida;
		private System.Windows.Forms.Label lblModulate;
		private Quake2.SetValue svModulate;
		private System.Windows.Forms.Label lblResolution;
		private Quake2.SetOption soResolution;
		private Quake2.SetValue svSensitivity;
		private System.Windows.Forms.TabPage abaAction;
		private System.Windows.Forms.Label lblBandAid;
		private Quake2.KeyBindBox kbbBandAid;
		private System.Windows.Forms.Label lblSensitivity;
		private Quake2.SetCheckBox scbCelShading;
		private System.Windows.Forms.Label lblCelShadingWidth;
		private Quake2.SetValue svCelShadingWidth;
		private System.Windows.Forms.Label lblConTrans;
		private Quake2.SetValue svConTrans;
		private System.Windows.Forms.Label lblShadows;
		private Quake2.SetOption soShadows;
		private System.Windows.Forms.TabPage abaVideo2;
		private System.Windows.Forms.Label lblFogDensity;
		private Quake2.SetValue svFogDensity;
		private Quake2.SetCheckBox scbFog;
		private System.Windows.Forms.GroupBox gbReplace;
		private Quake2.SetCheckBox scbReplacePcx;
		private Quake2.SetCheckBox scbReplaceWal;
		private Quake2.SetCheckBox scbReplaceMd2;
		private System.Windows.Forms.GroupBox gbDisplay;
		private Quake2.SetCheckBox scbDrawChatHud;
		private Quake2.SetCheckBox scbDrawFps;
		private Quake2.KeyBindBox kbbWeapon;
		private System.Windows.Forms.Label lblWeapon;
		private System.Windows.Forms.Label lblWaterWaves;
		private Quake2.SetCheckBox scbStainMaps;
		private Quake2.SetValue svWaterWaves;
		private Quake2.SetCheckBox scbXpFix;
		private Quake2.SetCheckBox scbRun;
		private Quake2.SetCheckBox scbMAccel;
		private System.Windows.Forms.SaveFileDialog saveFileDialog;
		private System.Windows.Forms.OpenFileDialog openFileDialog;
	}
}

