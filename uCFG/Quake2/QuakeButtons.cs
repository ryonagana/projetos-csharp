﻿using System;
using System.Collections;
using System.Windows.Forms;

namespace Quake2
{
	public static class QuakeButtons
	{
		static Hashtable _keyNames;

		public enum Mouse
		{
			Left,
			Right,
			Middle,
			Bt4,
			Bt5,
			WheelUp,
			WheelDown
		}

		private static void SetKeyNames()
		{
			_keyNames = new Hashtable();

			#region Mouse
			_keyNames.Add(Mouse.Left, "MOUSE1");
			_keyNames.Add(Mouse.Right, "MOUSE2");
			_keyNames.Add(Mouse.Middle, "MOUSE3");
			_keyNames.Add(Mouse.Bt4, "MOUSE4");
			_keyNames.Add(Mouse.Bt5, "MOUSE5");
			_keyNames.Add(Mouse.WheelUp, "MWHEELUP");
			_keyNames.Add(Mouse.WheelDown, "MWHEELDOWN");
			#endregion

			#region Setas
			_keyNames.Add(Keys.Up , "UPARROW");
			_keyNames.Add(Keys.Down, "DOWNARROW");
			_keyNames.Add(Keys.Left, "LEFTARROW");
			_keyNames.Add(Keys.Right, "RIGHTARROW");
			#endregion

			#region F<x>
			_keyNames.Add(Keys.F1, "F1");
			_keyNames.Add(Keys.F2, "F2");
			_keyNames.Add(Keys.F3, "F3");
			_keyNames.Add(Keys.F4, "F4");
			_keyNames.Add(Keys.F5, "F5");
			_keyNames.Add(Keys.F6, "F6");
			_keyNames.Add(Keys.F7, "F7");
			_keyNames.Add(Keys.F8, "F8");
			_keyNames.Add(Keys.F9, "F9");
			_keyNames.Add(Keys.F10, "F10");
			_keyNames.Add(Keys.F11, "F11");
			_keyNames.Add(Keys.F12, "F12");
			#endregion

			#region Letras
			_keyNames.Add(Keys.A, "A");
			_keyNames.Add(Keys.B, "B");
			_keyNames.Add(Keys.C, "C");
			_keyNames.Add(Keys.D, "D");
			_keyNames.Add(Keys.E, "E");
			_keyNames.Add(Keys.F, "F");
			_keyNames.Add(Keys.G, "G");
			_keyNames.Add(Keys.H, "H");
			_keyNames.Add(Keys.I, "I");
			_keyNames.Add(Keys.J, "J");
			_keyNames.Add(Keys.K, "K");
			_keyNames.Add(Keys.L, "L");
			_keyNames.Add(Keys.M, "M");
			_keyNames.Add(Keys.N, "N");
			_keyNames.Add(Keys.O, "O");
			_keyNames.Add(Keys.P, "P");
			_keyNames.Add(Keys.Q, "Q");
			_keyNames.Add(Keys.R, "R");
			_keyNames.Add(Keys.S, "S");
			_keyNames.Add(Keys.T, "T");
			_keyNames.Add(Keys.U, "U");
			_keyNames.Add(Keys.V, "V");
			_keyNames.Add(Keys.W, "W");
			_keyNames.Add(Keys.X, "X");
			_keyNames.Add(Keys.Y, "Y");
			_keyNames.Add(Keys.Z, "Z");
			#endregion

			#region Numeros
			_keyNames.Add(Keys.D0, "0");
			_keyNames.Add(Keys.D1, "1");
			_keyNames.Add(Keys.D2, "2");
			_keyNames.Add(Keys.D3, "3");
			_keyNames.Add(Keys.D4, "4");
			_keyNames.Add(Keys.D5, "5");
			_keyNames.Add(Keys.D6, "6");
			_keyNames.Add(Keys.D7, "7");
			_keyNames.Add(Keys.D8, "8");
			_keyNames.Add(Keys.D9, "9");
			#endregion

			#region Simbolos
			_keyNames.Add(Keys.Oemtilde, "~");
			_keyNames.Add(Keys.OemQuestion, "?");
			_keyNames.Add(Keys.OemOpenBrackets, "[");
			_keyNames.Add(Keys.OemCloseBrackets, "]");
			_keyNames.Add(Keys.OemMinus, "-");
			_keyNames.Add(Keys.Oemplus, "+");
			_keyNames.Add(Keys.Oemcomma, ",");
			_keyNames.Add(Keys.OemSemicolon, ";");
			#endregion

			#region Outras
			_keyNames.Add(Keys.Tab, "TAB");
			_keyNames.Add(Keys.Back, "BACKSPACE");
			_keyNames.Add(Keys.Enter, "ENTER");
			_keyNames.Add(Keys.ShiftKey, "SHIFT");
			_keyNames.Add(Keys.ControlKey, "CTRL");
			_keyNames.Add(Keys.Space, "SPACE");
			_keyNames.Add(Keys.Alt, "ALT");
			_keyNames.Add(Keys.PageDown, "PGDOWN");
			_keyNames.Add(Keys.PageUp, "PGUP");
			_keyNames.Add(Keys.End, "END");
			_keyNames.Add(Keys.Delete, "DEL");
			_keyNames.Add(Keys.Home, "HOME");
			_keyNames.Add(Keys.Insert, "INS");
			#endregion

			#region NumPad
			_keyNames.Add(Keys.NumPad0, "KP_0");
			_keyNames.Add(Keys.NumPad1, "KP_1");
			_keyNames.Add(Keys.NumPad2, "KP_2");
			_keyNames.Add(Keys.NumPad3, "KP_3");
			_keyNames.Add(Keys.NumPad4, "KP_4");
			_keyNames.Add(Keys.NumPad5, "KP_5");
			_keyNames.Add(Keys.NumPad6, "KP_6");
			_keyNames.Add(Keys.NumPad7, "KP_7");
			_keyNames.Add(Keys.NumPad8, "KP_8");
			_keyNames.Add(Keys.NumPad9, "KP_9");
			#endregion

			#region Nao Sei
			#endregion
		}

		public static string GetKeyName(object Key)
		{
			string nomeTecla;

			if (_keyNames == null)
				SetKeyNames();

			try
			{
				nomeTecla = _keyNames[Key].ToString().ToLower();
			}
			catch
			{
				nomeTecla = String.Empty;
			}

			return nomeTecla;
		}
	}
}
