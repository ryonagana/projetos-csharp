﻿using System;
using System.Windows.Forms;

namespace Quake2
{
	public class SetOption : ComboBox
	{
		#region Atributos
		ConfigFile.Set _set;
		#endregion

		#region Propriedades
		public ConfigFile.Set Set
		{
			get { return this._set; }
			set
			{
				this._set = value;
				this.ReLoad();
			}
		}
		public string SetCommand
		{
			get
			{
				if (this._set == null)
					return String.Empty;
				return this._set.Option;
			}
			set
			{
				if (this._set == null)
					this._set = new ConfigFile.Set(value);
				else
					this._set.Option = value;
			}
		}
		#endregion

		#region Construtores
		public SetOption()
		{
			this.SelectionChangeCommitted += new EventHandler(this._ValueChanged);
		}
		#endregion

		#region Eventos
		void _ValueChanged(object sender, EventArgs e)
		{
			this._set.Value = Convert.ToString((sender as ComboBox).SelectedIndex);
		}
		#endregion

		#region Métodos
		public void ReLoad()
		{
			if (this._set != null)
				this.SelectedIndex = Convert.ToInt32(this._set.Value);
		}
		#endregion
	}
}
