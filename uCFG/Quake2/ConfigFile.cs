using System;
using System.IO;
using System.Collections;

namespace Quake2
{
	public partial class ConfigFile
	{
		#region Atributos
		Hashtable _sections;
		#endregion

		#region Propriedades
		public ICollection Sections
		{
			get { return this._sections.Values; }
		}
		public Section this[string sectionName]
		{
			get
			{
				if (this._sections.ContainsKey(sectionName))
					return this._sections[sectionName] as Section;

				ConfigFile.Section newSection = new ConfigFile.Section(sectionName);
				this._sections.Add(sectionName, newSection);
				return newSection;
			}
		}
		#endregion

		#region Construtores
		public ConfigFile()
		{
			this._sections = new Hashtable();
		}
		public ConfigFile(string fileName)
		{
			this._sections = new Hashtable();
			try
			{
				this.Load(fileName);
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
		#endregion

		#region Métodos
		#region Load / Save / Clear
		public void Load(string fileName)
		{
			StreamReader file = new StreamReader(fileName);
			try
			{
				ReadSections(file);
			}
			catch (Exception ex)
			{
				throw ex;
			}
			finally
			{
				file.Close();
			}
		}
		public void Save(string fileName)
		{
			StreamWriter file = new StreamWriter(fileName);
			try
			{
				WriteSections(file);
			}
			catch (Exception ex)
			{
				throw ex;
			}
			finally
			{
				file.Close();
			}
		}
		public void Clear()
		{
			this._sections = new Hashtable();
		}
		#endregion
		#region Read / Write
		private void ReadSections(StreamReader file)
		{
			int espaco, i;
			bool branco = true;
			string linha, tipo, valor;
			Section actSec;

			if (this._sections.ContainsKey("Default"))
				actSec = this._sections["Default"] as Section;
			else
			{
				actSec = new Section("Default");
				this._sections.Add("Default", actSec);
			}

			while (!file.EndOfStream)
			{
				linha = file.ReadLine();

				#region Identifica Grupos
				if (linha.StartsWith("// ") && branco)
				{
					if (this._sections.ContainsKey(linha.Substring(3)))
						actSec = this._sections[linha.Substring(3)] as Section;
					else
					{
						actSec = new Section(linha.Substring(3));
						this._sections.Add(linha.Substring(3), actSec);
					}
					continue;
				}
				#endregion

				// Apaga Comentarios
				if (linha.Contains("//"))
					linha = linha.Substring(0, linha.IndexOf("//"));

				if (linha.Equals(String.Empty))
				{
					branco = true;
					continue;
				}

				for(i = 0, espaco = 0; espaco < 2 && i < linha.Length; i++)
				{
					if(linha[i] == ' ')
						espaco++;
				}

				tipo = linha.Split(' ')[0];
				valor = linha.Substring(i);

				if(!valor.Equals(String.Empty))
				{
                    if (valor[0].Equals('\"'))
                        valor = valor.Substring(1, valor.IndexOf('\"', 1) - 1);
                    else
                    {
                        valor = valor.Trim(' ');
                        valor = valor.Trim('\t');
                    }
				}

				switch (tipo)
				{
					case "bind":
						if(espaco.Equals(2))
							actSec.Add(new Bind(linha.Split(' ')[1], valor));
						break;
					case "set":
						if (espaco.Equals(2))
							actSec.Add(new Set(linha.Split(' ')[1], valor));
						break;
					case "alias": //Apoprósitio
						//actSec.Add(new Alias(linha[1], linha[2]));
						break;
					default:
						if(espaco.Equals(1))
							actSec.Add(new Set(linha.Split(' ')[0], linha.Split(' ')[1]));
						break;
				}
				branco = false;
			}
		}
		private void WriteSections(StreamWriter file)
		{
			foreach (Section sec in this._sections.Values)
			{
				file.WriteLine("// {0}", sec.Name);

				// Salva binds
				foreach (Bind bind in sec.Binds)
				{
					if (!bind.Key.Equals(String.Empty))
					{
						foreach (string key in bind.Keys)
						{
							file.WriteLine("bind {0} \"{1}\"", key, bind.Action);
						}
					}
				}

				// Salva configurações
				foreach (Set set in sec.Sets)
				{
					if (!set.Value.Equals(String.Empty))
					{
						file.WriteLine("set {0} \"{1}\"", set.Option, set.Value);
					}
				}

				// Salva novos comandos
				foreach (Alias alias in sec.Aliases)
				{
					if (!alias.CmdActivate.Equals(String.Empty))
					{
						if (!alias.CmdDeactivate.Equals(String.Empty))
						{
							file.WriteLine("alias +{0} \"{1}\"", alias.Name, alias.CmdActivate);
							file.WriteLine("alias -{0} \"{1}\"", alias.Name, alias.CmdDeactivate);
						}
						else
						{
							file.WriteLine("alias {0} \"{1}\"", alias.Name, alias.CmdActivate);
						}
					}
				}

				file.WriteLine();
			}
		}
		#endregion
		#endregion
	}
}