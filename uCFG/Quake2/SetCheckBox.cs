﻿using System;
using System.Windows.Forms;

namespace Quake2
{
	public class SetCheckBox : CheckBox
	{
		#region Atributos
		ConfigFile.Set _set;
		#endregion

		#region Propriedades
		public ConfigFile.Set Set
		{
			get { return this._set; }
			set
			{
				this._set = value;
				this.ReLoad();
			}
		}
		public string SetCommand
		{
			get
			{
				return this._set.Option;
			}
			set
			{
				this._set.Option = value;
			}
		}
		#endregion

		#region Construtores
		public SetCheckBox()
		{
			this._set = new ConfigFile.Set(null, "0");
			this.CheckedChanged += new EventHandler(this._CheckedChanged);
		}
		#endregion

		#region Eventos
		void _CheckedChanged(object sender, EventArgs e)
		{
			if ((sender as CheckBox).Checked)
				this._set.Value = "1";
			else
				this._set.Value = "0";
		}
		#endregion

		#region Métodos
		public void ReLoad()
		{
			if (this._set != null)
				this.Checked = this._set.Value.Equals("1");
		}
		#endregion
	}
}
