using System.Collections;

namespace Quake2
{
	public partial class ConfigFile
	{
		public class Section
		{
			#region Atributos
			string _name;
			Hashtable _binds;
			Hashtable _sets;
			Hashtable _aliases;
			#endregion

			#region Propriedades
			public string Name
			{
				get { return this._name; }
				set { this._name = value; }
			}
			public ICollection Binds
			{
				get { return this._binds.Values; }
			}
			public ICollection Sets
			{
				get { return this._sets.Values; }
			}
			public ICollection Aliases
			{
				get { return this._aliases.Values; }
			}
			#endregion

			#region Construtores
			public Section(string name)
			{
				this._name = name;
				this._binds = new Hashtable();
				this._sets = new Hashtable();
				this._aliases = new Hashtable();
			}
			#endregion

			#region M�todos
			public bool ContainsBind(string action)
			{
				return this._binds.ContainsKey(action);
			}
			public Bind GetBind(string action)
			{
				return this._binds[action] as Bind;
			}
			public bool ContainsSet(string option)
			{
				return this._sets.ContainsKey(option);
			}
			public Set GetSet(string option)
			{
				return this._sets[option] as Set;
			}
			public bool ContainsAlias(string name)
			{
				return this._binds.ContainsKey(name);
			}
			public Alias GetAlias(string name)
			{
				return this._aliases[name] as Alias;
			}
			public Set Add(Set nSet)
			{
				if (this._sets.ContainsKey(nSet.Option))
					(this._sets[nSet.Option] as Set).Value = nSet.Value;
				else
					this._sets.Add(nSet.Option, nSet);
				return nSet;
			}
			public Bind Add(Bind nBind)
			{
				if (this._binds.ContainsKey(nBind.Action))
				{
					(this._binds[nBind.Action] as Bind).AddKeys(nBind.Keys);
				}
				else
				{
					this._binds.Add(nBind.Action, nBind);
				}
				return nBind;
			}
			public Alias Add(Alias nAlias)
			{
				this._aliases.Add(nAlias.Name, nAlias);
				return nAlias;
			}
			#endregion
		}
	}
}
