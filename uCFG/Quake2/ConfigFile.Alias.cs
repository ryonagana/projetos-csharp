namespace Quake2
{
	public partial class ConfigFile
	{
		public class Alias
		{
			#region Atributos
			string _nome;
			string _cmdAtiva;
			string _cmdDesativa;
			#endregion

			#region Propriedades
			public string Name
			{
				get { return this._nome; }
				set { this._nome = value; }
			}
			public string CmdActivate
			{
				get { return this._cmdAtiva; }
				set { this._cmdAtiva = value; }
			}
			public string CmdDeactivate
			{
				get { return this._cmdDesativa; }
				set { this._cmdDesativa = value; }
			}
			#endregion

			#region Construtores
			public Alias(string nome, string cmdAtiva, string cmdDesativa)
			{
				this._nome = nome;
				this._cmdAtiva = cmdAtiva;
				this._cmdDesativa = cmdDesativa;
			}
			#endregion

			#region M�todos
			#endregion
		}
	}
}
