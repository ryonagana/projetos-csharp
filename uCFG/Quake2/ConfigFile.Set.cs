using System;
namespace Quake2
{
	public partial class ConfigFile
	{
		public class Set
		{
			#region Atributos
			string _option;
			string _value;
			#endregion

			#region Propriedades
			public string Option
			{
				get { return this._option; }
				set { this._option = value; }
			}
			public string Value
			{
				get { return this._value; }
				set { this._value = value; }
			}
			#endregion

			#region Construtores
			public Set(string option)
			{
				this._option = option;
				this._value = String.Empty;
			}
			public Set(string option, string value)
			{
				this._option = option;
				this._value = value;
			}
			#endregion

			#region M�todos
			#endregion
		}
	}
}
