using System;
using System.Windows.Forms;

namespace Quake2
{
	public class KeyBindBox : TextBox
	{
		#region Atributos
		bool _detectKeys;
		ConfigFile.Bind _bind;
		#endregion

		#region Propriedades
		public bool DetectKeys
		{
			get { return this._detectKeys; }
			set { this._detectKeys = value; }
		}
		public ConfigFile.Bind Bind
		{
			get { return this._bind; }
			set
			{
				this._bind = value;
				this.ReLoad();
			}
		}
		public string BindAction
		{
			get
			{
				if(this._bind == null)
					return String.Empty;	
				return this._bind.Action;
			}
			set
			{
				if (this._bind == null)
					this._bind = new ConfigFile.Bind(value);
				else
					this._bind.Action = value;
			}
		}
		#endregion

		#region Construtores
		public KeyBindBox()
		{
			// Desabilita menu de botao direito na caixa
			this.ContextMenu = new ContextMenu();

			// Associa evento de mouse na TextBox
			this.MouseWheel += new MouseEventHandler(this._tbMouseEvent);
			this.MouseDown += new MouseEventHandler(this._tbMouseEvent);

			// Associa evento de teclado na TextBox
			this.KeyDown += new KeyEventHandler(this._tbKeyEvent);
            this.KeyUp += new KeyEventHandler(this._tbKeyUp);

			this.GotFocus += new EventHandler(_tbGotFocus);

			// Ativa detec��o por padr�o
			this._detectKeys = true;
		}
		#endregion

		#region Eventos
		void _tbGotFocus(object sender, EventArgs e)
		{
			this.Select(0, 0);
		}
		// Apertou bot�o do Mouse
		private void _tbMouseEvent(object sender, MouseEventArgs e)
		{
			string nomeBotao = null;
			switch (e.Button)
			{
				case MouseButtons.Left:
					if (e.Clicks > 1)
						nomeBotao = QuakeButtons.GetKeyName(QuakeButtons.Mouse.Left);
					else
						this._detectKeys = true;
					break;
				case MouseButtons.Right:
					nomeBotao = QuakeButtons.GetKeyName(QuakeButtons.Mouse.Right);
					break;
				case MouseButtons.Middle:
					nomeBotao = QuakeButtons.GetKeyName(QuakeButtons.Mouse.Middle);
					break;
				case MouseButtons.XButton1:
					nomeBotao = QuakeButtons.GetKeyName(QuakeButtons.Mouse.Bt4);
					break;
				case MouseButtons.XButton2:
					nomeBotao = QuakeButtons.GetKeyName(QuakeButtons.Mouse.Bt5);
					break;
				default:
					if (e.Delta > 0)
						nomeBotao = QuakeButtons.GetKeyName(QuakeButtons.Mouse.WheelUp);
					else if (e.Delta < 0)
						nomeBotao = QuakeButtons.GetKeyName(QuakeButtons.Mouse.WheelDown);
					break;
			}
			if (nomeBotao != null)
			{
				this.Text = nomeBotao;
				this._bind.Key = nomeBotao;

				SelectNextParentControl();
			}
		}
		// Apertou Tecla
		private void _tbKeyEvent(object sender, KeyEventArgs e)
		{
			if (this.SelectionLength > 0)
				this._detectKeys = false;

			if (this._detectKeys)
			{
				if (this._bind != null)
				{
					if (e.Alt)
					{
						this.Text = QuakeButtons.GetKeyName(Keys.Alt);
					}
					else
					{
						this.Text = QuakeButtons.GetKeyName(e.KeyCode);
					}

					this._bind.Key = this.Text;
					SelectNextParentControl();
				}

				e.SuppressKeyPress = true;
			}
		}
        void _tbKeyUp(object sender, KeyEventArgs e)
        {
            this._bind.Key = this.Text;
        }
		// Capturar todas as teclas
		protected override bool IsInputKey(Keys keyData)
		{
			return true;
		}
		// Mandar foco para pr�ximo Control
		private void SelectNextParentControl()
		{
			try
			{
				//Seleciona pr�ximo Control
				this.Parent.SelectNextControl(this, true, false, true, true);
			}
			catch { }
		}
		#endregion

		#region Metodos
		public void ReLoad()
		{
			if (this._bind != null)
				this.Text = this._bind.Key;
		}
		#endregion
	}
}
