using System;
using System.Collections.Generic;
namespace Quake2
{
	public partial class ConfigFile
	{
		public class Bind
		{
			#region Atributos
			List<string> _teclas;
			string _comando;
			#endregion

			#region Propriedades
			public string Key
			{
				get
				{
					if (this._teclas.Count.Equals(0))
						return String.Empty;
					return this._teclas[0];
				}
				set
				{
					this._teclas.Clear();
					this._teclas.Insert(0, value);
				}
			}
			public List<string> Keys
			{
				get { return this._teclas; }
				set
				{
					if (value == null)
						this._teclas = new List<string>();
					else
						this._teclas = value;
				}
			}
			public string Action
			{
				get { return this._comando; }
				set { this._comando = value; }
			}
			#endregion

			#region Construtores
			public Bind(string comando)
			{
				this._comando = comando;
				this._teclas = new List<string>();
			}
			public Bind(string tecla, string comando)
			{
				this._teclas = new List<string>();
				this._teclas.Insert(0, tecla);
				this._comando = comando;
			}
			public Bind(List<string> teclas, string comando)
			{
				this.Keys = teclas;
				this._comando = comando;
			}
			#endregion

			#region M�todos
			public void AddKeys(List<string> keys)
			{
				foreach(string key in keys)
					if (!this._teclas.Contains(key))
						this._teclas.Insert(this._teclas.Count, key);
			}
			#endregion
		}
	}
}