﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Quake2;

namespace TestConsole
{
	class Program
	{
		static void Main(string[] args)
		{
			try
			{
				ConfigFile cfg = new ConfigFile("kal.ucfg");
				cfg.Save("config_teste.cfg");
			}
			catch (Exception ex)
			{
				Console.WriteLine("Erro: {0}", ex.Message);
				Console.ReadKey();
			}
		}
	}
}
