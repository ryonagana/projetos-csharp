﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Quake2;

namespace uCFG
{
	public partial class Form1 : Form
	{
		ConfigFile _cfg;
		
		public Form1()
		{
			InitializeComponent();
			this._cfg = new ConfigFile();
			IniciaValores();
		}

		private void CriaAliasesPadrao()
		{
			this._cfg["Alias"].Add(new ConfigFile.Alias("atirar", "+attack;+use", "-attack;-use"));
			this._cfg["Alias"].Add(new ConfigFile.Alias("bandaid", "bandage", "bandage"));
		}

		#region Ler / Salvar Binds
		public void IniciaValores()
		{
			CriaAliasesPadrao();
			// Preenche campos com valores da config
			LoadBinds();
			LoadSets();
		}

		private void SaveSets()
		{
			#region Movimentação
			AddSet(scbRun, "Movimentação");
			#endregion

			#region Armas
			AddSet(scbMAccel, "Uso - Armas");
			AddSet(scbXpFix, "Uso - Armas");
			#endregion

			#region Vídeo
			AddSet(scbLuzColorida, "Vídeo");
			AddSet(scbCelShading, "Vídeo");
			AddSet(svCelShadingWidth, "Vídeo");
			AddSet(svConTrans, "Vídeo");
			AddSet(soShadows, "Vídeo");
			AddSet(svModulate, "Vídeo");
			AddSet(soResolution, "Vídeo");
			#endregion

			#region Vídeo 2
			AddSet(svFogDensity, "Vídeo");
			AddSet(scbFog, "Vídeo");

			AddSet(scbReplaceMd2, "Vídeo");
			AddSet(scbReplacePcx, "Vídeo");
			AddSet(scbReplaceWal, "Vídeo");

			AddSet(scbDrawFps, "Display");
			AddSet(scbDrawChatHud, "Display");

			AddSet(scbStainMaps, "Vídeo");
			AddSet(svWaterWaves, "Vídeo");
			#endregion
		}
		private void LoadSets()
		{
			#region Movimentação
			LoadSet(scbRun, "Movimentação", "cl_run");
			#endregion

			#region Armas
			LoadSet(scbXpFix, "Uso - Armas", "m_xpfix");
			LoadSet(scbMAccel, "Uso - Armas", "m_accel");
			#endregion

			#region Video
			LoadSet(scbLuzColorida, "Vídeo", "gl_coloredlightmaps");
			LoadSet(scbCelShading, "Vídeo", "gl_celshading");
			LoadSet(svCelShadingWidth, "Vídeo", "gl_celshading_width");
			LoadSet(svConTrans, "Vídeo", "gl_contrans");
			LoadSet(soShadows, "Vídeo", "gl_shadows");
			LoadSet(svModulate, "Vídeo", "gl_modulate");
			LoadSet(soResolution, "Vídeo", "gl_mode");
			#endregion

			#region Vídeo 2
			LoadSet(svFogDensity, "Vídeo", "gl_fog_density");
			LoadSet(scbFog, "Vídeo", "gl_fog");

			LoadSet(scbReplaceMd2, "Vídeo", "gl_replacemd2");
			LoadSet(scbReplacePcx, "Vídeo", "gl_replacepcx");
			LoadSet(scbReplaceWal, "Vídeo", "gl_replacewal");

			LoadSet(scbDrawChatHud, "Display", "cl_chathud");
			LoadSet(scbDrawFps, "Display", "cl_fps");

			LoadSet(scbStainMaps, "Vídeo", "gl_stainmaps");
			LoadSet(svWaterWaves, "Vídeo", "gl_waterwaves");
			#endregion
		}
		private void SaveBinds()
		{
			#region Movimentaçao
			AddBind(kbbBack, "Movimentação");
			AddBind(kbbMoveUp, "Movimentação");
			AddBind(kbbForward, "Movimentação");
			AddBind(kbbMoveLeft, "Movimentação");
			AddBind(kbbMoveRight, "Movimentação");
			#endregion

			#region Armas
			AddSet(svSensitivity, "Uso - Armas");
			AddBind(kbbAtirar, "Uso - Armas");
			AddBind(kbbReload, "Uso - Armas");
			#endregion

			#region Action
			AddBind(kbbBandAid, "Action");
			AddBind(kbbWeapon, "Action");
			#endregion
		}
		private void LoadBinds()
		{
			#region Movimentaçao
			LoadBind(kbbBack, "Movimentação", "+back");
			LoadBind(kbbMoveUp, "Movimentação", "+moveup");
			LoadBind(kbbForward, "Movimentação", "+forward");
			LoadBind(kbbMoveLeft, "Movimentação", "+moveleft");
			LoadBind(kbbMoveRight, "Movimentação", "+moveright");
			#endregion

			#region Armas
			LoadSet(svSensitivity, "Uso - Armas", "sensitivity");
			LoadBind(kbbAtirar, "Uso - Armas", "+atirar");
			LoadBind(kbbReload, "Uso - Armas", "reload");
			#endregion

			#region Action
			LoadBind(kbbBandAid, "Action", "+bandaid");
			LoadBind(kbbWeapon, "Action", "weapon");
			#endregion
		}

		#region Auxiliares
		private void LoadBind(KeyBindBox kbb, string sessao, string action)
		{
			bool nAchou = true;
			foreach(ConfigFile.Section sec in this._cfg.Sections)
			{
				// Se bind está na config, entao ele vai para o KeyBindBox
				if (sec.ContainsBind(action))
				{
					kbb.Bind = sec.GetBind(action);
					nAchou = false;
				}
			}

			if(nAchou)
				kbb.BindAction = action;
		}
		private void AddBind(KeyBindBox kbb, string sessao)
		{
			this._cfg[sessao].Add(kbb.Bind);
		}

		private void LoadSet(SetCheckBox set, string sessao, string option)
		{
			bool nAchou = true;

			foreach (ConfigFile.Section sec in this._cfg.Sections)
			{
				// Se set está na config, entao ele vai para o SetCheckBox
				if (sec.ContainsSet(option))
				{
					set.Set = sec.GetSet(option);
					nAchou = false;
				}
			}

			if (nAchou)
				set.SetCommand = option;
		}
		private void AddSet(SetCheckBox scb, string sessao)
		{
			this._cfg[sessao].Add(scb.Set);
		}
		private void LoadSet(SetValue set, string sessao, string option)
		{
			bool nAchou = true;

			foreach (ConfigFile.Section sec in this._cfg.Sections)
			{
				// Se set está na config, entao ele vai para o SetCheckBox
				if (sec.ContainsSet(option))
				{
					set.Set = sec.GetSet(option);
					nAchou = false;
				}
			}

			if (nAchou)
				set.SetCommand = option;
		}
		private void AddSet(SetValue sv, string sessao)
		{
			this._cfg[sessao].Add(sv.Set);
		}
		private void LoadSet(SetOption set, string sessao, string option)
		{
			bool nAchou = true;

			foreach (ConfigFile.Section sec in this._cfg.Sections)
			{
				// Se set está na config, entao ele vai para o SetCheckBox
				if (sec.ContainsSet(option))
				{
					set.Set = sec.GetSet(option);
					nAchou = false;
				}
			}

			if (nAchou)
				set.SetCommand = option;
		}
		private void AddSet(SetOption so, string sessao)
		{
			this._cfg[sessao].Add(so.Set);
		}
		
		#endregion
		#endregion

		#region Botoes
		private void btnSalvar_Click(object sender, EventArgs e)
		{
			// Adiciona Binds à config e salva
			SaveBinds();
			SaveSets();

			if(saveFileDialog.ShowDialog() == DialogResult.OK)
				this._cfg.Save(saveFileDialog.FileName);
		}
		private void btnCarregar_Click(object sender, EventArgs e)
		{
			if (openFileDialog.ShowDialog() == DialogResult.OK)
			{
				this._cfg = new ConfigFile(openFileDialog.FileName);
				this.IniciaValores();
			}
		}
		#endregion

		private void scbCelShading_CheckedChanged(object sender, EventArgs e)
		{
			this.svCelShadingWidth.Enabled = (sender as CheckBox).Checked;
			this.lblCelShadingWidth.Enabled = (sender as CheckBox).Checked;
		}

		private void scbFog_CheckedChanged(object sender, EventArgs e)
		{
			this.svFogDensity.Enabled = (sender as CheckBox).Checked;
			this.lblFogDensity.Enabled = (sender as CheckBox).Checked;
		}
	}
}
