﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace TerrariaEd.Window
{
    class FileWindow : Form
    {
        private TabControl ConfigTab;
        private TabPage tabInternoConfig;
        private GroupBox gpData;
        private GroupBox gpBoxMain;
        private TextBox cpNameFile;
        private Label label1;
        private Label lbMaxHealth;
        private NumericUpDown numSetMaxLife;
        private Label label3;
        private Label label2;
        private Label lbHealth;
        private TabPage tabInternoInven;
    
        public string Filename { get; set; }
        public string Filepath { get; set; }

        FileStream fileData;

        PlayerRead pr = new PlayerRead();
        private Label label5;
        private TextBox cpSetLife;
        private TextBox cpVersion;
        private Label label6;
        GatherData data = null;

        

        public FileWindow(string filename) : base()
        {
            InitializeComponent();
            Filename = Path.GetFileName(filename);
            this.Text = Path.GetFileName(filename);
            Filepath = Path.GetFullPath(filename);


            pr.TerrariaCrypt.Decrypt(Filepath, Filepath);
            pr.TerrariaCrypt.LoadDecryptedToMemory(Filename + ".decrypted");

            data = new GatherData(Filename + ".decrypted");
            
            //data.getName();
            //data.GetVersion();
            //data.GetLife();

            cpVersion.Text = String.Format("{0}", data.Player.ver);
            cpNameFile.Text = data.Player.name;
            lbHealth.Text = String.Format("{0}", data.Player.actualLife);

            cpSetLife.Text = String.Format("{0}", data.Player.actualLife);
            cpSetLife.LostFocus += new EventHandler(cpSetLife_TextChanged);

            numSetMaxLife.Value = Convert.ToDecimal(data.Player.actualLife);
            numSetMaxLife.Maximum = 400;
            
            //lbMaxHealth.Text = String.Format("/{0}", data.Player.maxLife); 

            //data.CloseStream();






        }

        void cpSetLife_TextChanged(object sender, EventArgs e)
        {
            int val;

            try
            {

                if (Convert.ToInt32(cpSetLife.Text) > data.Player.maxLife)
                {
                    MessageBox.Show("You cant put a value greater than max value: " + data.Player.maxLife +   "\n\nPlease Set Max Value First", "What?", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            catch { }
            //throw new NotImplementedException();
        }

        private void InitializeComponent()
        {
            this.ConfigTab = new System.Windows.Forms.TabControl();
            this.tabInternoConfig = new System.Windows.Forms.TabPage();
            this.gpData = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cpSetLife = new System.Windows.Forms.TextBox();
            this.lbHealth = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lbMaxHealth = new System.Windows.Forms.Label();
            this.numSetMaxLife = new System.Windows.Forms.NumericUpDown();
            this.gpBoxMain = new System.Windows.Forms.GroupBox();
            this.cpVersion = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cpNameFile = new System.Windows.Forms.TextBox();
            this.tabInternoInven = new System.Windows.Forms.TabPage();
            this.ConfigTab.SuspendLayout();
            this.tabInternoConfig.SuspendLayout();
            this.gpData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSetMaxLife)).BeginInit();
            this.gpBoxMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // ConfigTab
            // 
            this.ConfigTab.Controls.Add(this.tabInternoConfig);
            this.ConfigTab.Controls.Add(this.tabInternoInven);
            this.ConfigTab.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ConfigTab.Location = new System.Drawing.Point(0, 0);
            this.ConfigTab.Name = "ConfigTab";
            this.ConfigTab.SelectedIndex = 0;
            this.ConfigTab.Size = new System.Drawing.Size(268, 348);
            this.ConfigTab.TabIndex = 0;
            // 
            // tabInternoConfig
            // 
            this.tabInternoConfig.Controls.Add(this.gpData);
            this.tabInternoConfig.Controls.Add(this.gpBoxMain);
            this.tabInternoConfig.Location = new System.Drawing.Point(4, 22);
            this.tabInternoConfig.Name = "tabInternoConfig";
            this.tabInternoConfig.Padding = new System.Windows.Forms.Padding(3);
            this.tabInternoConfig.Size = new System.Drawing.Size(260, 322);
            this.tabInternoConfig.TabIndex = 0;
            this.tabInternoConfig.Text = "Config";
            this.tabInternoConfig.UseVisualStyleBackColor = true;
            // 
            // gpData
            // 
            this.gpData.Controls.Add(this.label5);
            this.gpData.Controls.Add(this.cpSetLife);
            this.gpData.Controls.Add(this.lbHealth);
            this.gpData.Controls.Add(this.label3);
            this.gpData.Controls.Add(this.label2);
            this.gpData.Controls.Add(this.lbMaxHealth);
            this.gpData.Controls.Add(this.numSetMaxLife);
            this.gpData.Location = new System.Drawing.Point(8, 135);
            this.gpData.Name = "gpData";
            this.gpData.Size = new System.Drawing.Size(244, 179);
            this.gpData.TabIndex = 1;
            this.gpData.TabStop = false;
            this.gpData.Text = "Stats";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(11, 39);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Set Life";
            // 
            // cpSetLife
            // 
            this.cpSetLife.Location = new System.Drawing.Point(81, 39);
            this.cpSetLife.Name = "cpSetLife";
            this.cpSetLife.Size = new System.Drawing.Size(100, 20);
            this.cpSetLife.TabIndex = 7;
            // 
            // lbHealth
            // 
            this.lbHealth.AutoSize = true;
            this.lbHealth.Location = new System.Drawing.Point(78, 20);
            this.lbHealth.Name = "lbHealth";
            this.lbHealth.Size = new System.Drawing.Size(35, 13);
            this.lbHealth.TabIndex = 6;
            this.lbHealth.Text = "label5";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Set Max Life:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Health:";
            // 
            // lbMaxHealth
            // 
            this.lbMaxHealth.AutoSize = true;
            this.lbMaxHealth.Location = new System.Drawing.Point(143, 67);
            this.lbMaxHealth.Name = "lbMaxHealth";
            this.lbMaxHealth.Size = new System.Drawing.Size(33, 13);
            this.lbMaxHealth.TabIndex = 5;
            this.lbMaxHealth.Text = "/ 400";
            // 
            // numSetMaxLife
            // 
            this.numSetMaxLife.Increment = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.numSetMaxLife.Location = new System.Drawing.Point(81, 65);
            this.numSetMaxLife.Maximum = new decimal(new int[] {
            400,
            0,
            0,
            0});
            this.numSetMaxLife.Name = "numSetMaxLife";
            this.numSetMaxLife.Size = new System.Drawing.Size(56, 20);
            this.numSetMaxLife.TabIndex = 4;
            // 
            // gpBoxMain
            // 
            this.gpBoxMain.Controls.Add(this.cpVersion);
            this.gpBoxMain.Controls.Add(this.label6);
            this.gpBoxMain.Controls.Add(this.label1);
            this.gpBoxMain.Controls.Add(this.cpNameFile);
            this.gpBoxMain.Location = new System.Drawing.Point(8, 6);
            this.gpBoxMain.Name = "gpBoxMain";
            this.gpBoxMain.Size = new System.Drawing.Size(244, 123);
            this.gpBoxMain.TabIndex = 0;
            this.gpBoxMain.TabStop = false;
            this.gpBoxMain.Text = "Data:";
            // 
            // cpVersion
            // 
            this.cpVersion.Location = new System.Drawing.Point(12, 29);
            this.cpVersion.MaxLength = 20;
            this.cpVersion.Name = "cpVersion";
            this.cpVersion.ReadOnly = true;
            this.cpVersion.Size = new System.Drawing.Size(217, 20);
            this.cpVersion.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "File Version";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 52);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(131, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Name (max 20 characters)";
            // 
            // cpNameFile
            // 
            this.cpNameFile.Location = new System.Drawing.Point(12, 68);
            this.cpNameFile.MaxLength = 20;
            this.cpNameFile.Name = "cpNameFile";
            this.cpNameFile.Size = new System.Drawing.Size(217, 20);
            this.cpNameFile.TabIndex = 0;
            // 
            // tabInternoInven
            // 
            this.tabInternoInven.Location = new System.Drawing.Point(4, 22);
            this.tabInternoInven.Name = "tabInternoInven";
            this.tabInternoInven.Padding = new System.Windows.Forms.Padding(3);
            this.tabInternoInven.Size = new System.Drawing.Size(260, 322);
            this.tabInternoInven.TabIndex = 1;
            this.tabInternoInven.Text = "Inventory";
            this.tabInternoInven.UseVisualStyleBackColor = true;
            // 
            // FileWindow
            // 
            this.ClientSize = new System.Drawing.Size(268, 348);
            this.Controls.Add(this.ConfigTab);
            this.Name = "FileWindow";
            this.Load += new System.EventHandler(this.FileWindow_Load);
            this.ConfigTab.ResumeLayout(false);
            this.tabInternoConfig.ResumeLayout(false);
            this.gpData.ResumeLayout(false);
            this.gpData.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSetMaxLife)).EndInit();
            this.gpBoxMain.ResumeLayout(false);
            this.gpBoxMain.PerformLayout();
            this.ResumeLayout(false);

        }

        private void FileWindow_Load(object sender, EventArgs e)
        {
            
        }
    }
}
