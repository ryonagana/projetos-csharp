﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace TerrariaEd
{
    public partial class Form1 : Form
    {
        PlayerRead pread = new PlayerRead();
        BackgroundWorker bw = new BackgroundWorker();
        BackgroundWorker saveWork = new BackgroundWorker();
        Dictionary<string, Window.FileWindow> FileWindows = new Dictionary<string, Window.FileWindow>();
      

        public Form1()
        {
            InitializeComponent();
            this.IsMdiContainer = true;
            this.LayoutMdi(MdiLayout.Cascade);

     

        }

      
       

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void loadPlayerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog op = new OpenFileDialog();
            op.Filter = "Terraria Player File (*.plr)|*.plr|Terraria Player File Backup (*.plr.bak)|*.plr.bak";

            

            if (op.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {

                try
                {
                    string fname = Path.GetFileName(op.FileName);
                    FileWindows.Add(fname, new Window.FileWindow(op.FileName));
                    FileWindows[fname].MdiParent = this;
                    FileWindows[fname].Show();
                    //FileWindows[fname].FormClosed += new FormClosedEventHandler(Form1_FormClosed);
                    //FileWindows[fname].GotFocus += new EventHandler(Form1_GotFocus);

                }
                catch (Exception file)
                {
                    MessageBox.Show("A file With the same name was added! This Error is Caused By: " + file.Message + "\n\nCode: " + file.Source, "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);

                }
              

               
            }

        }

        void Form1_GotFocus(object sender, EventArgs e)
        {
            MessageBox.Show("focused");
            //throw new NotImplementedException();
        }

        void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            
            //throw new NotImplementedException();
        }

        

       
    }
}
