﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Security.Cryptography;

namespace TerrariaEd
{
    class PlayerRead
    {

        public TerrariaCrypt TerrariaCrypt { get; set; }

        public FileStream fin { get; set; }
        

        public bool UnecryptedLoaded = false;
        public bool CryptedLoaded = false;


        public delegate void UncryptedFileLoaded(EventArgs e);
        public delegate void CryptedFileLoaded(EventArgs e);

        public event UncryptedFileLoaded OnUncryptedFileLoaded;
        public event CryptedFileLoaded OnCryptedFileLoaded;

        public PlayerRead()
        {
            TerrariaCrypt = new TerrariaCrypt();

        }

        public void LoadEncryptedFile(string filepath)
        {
            if (File.Exists(filepath))
            {
               
                TerrariaCrypt.Decrypt(filepath, filepath);

                if (OnCryptedFileLoaded != null)
                {
                    EventArgs e = new EventArgs();
                    CryptedLoaded = true;
                    OnCryptedFileLoaded.Invoke(e);
                    
                }

            }
        }

        public void LoadDecryptedFile(string filepath){


            try
            {
                if (File.Exists(filepath))
                {
                    fin = new FileStream(filepath, FileMode.Open);

                }

                if (OnUncryptedFileLoaded != null)
                {
                    EventArgs e = new EventArgs();
                    UnecryptedLoaded = true;
                    OnUncryptedFileLoaded.Invoke(e);
                }

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error:");

            }
            

                
            

        }

    }
}
