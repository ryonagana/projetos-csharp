﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.IO;

namespace TerrariaEd
{
    public class TerrariaCrypt
    {
         UnicodeEncoding unicodeEncode = new UnicodeEncoding();
         byte[] data;
         RijndaelManaged algorythm = new RijndaelManaged();
         CryptoStream cryptStream;
         FileStream finput;
         FileStream foutput;

            
         

        public TerrariaCrypt()
        {

            data = Encoding.Unicode.GetBytes("h3y_gUyZ");
            

        }

        public  void Encrypt(FileStream fs, string output){


            string[] cut = output.Split('.');

            string filename = cut[0] + "." + cut[1];
            byte[] datafile;

            foutput = new FileStream(filename, FileMode.Create);

            ICryptoTransform encrypt = algorythm.CreateEncryptor(data, data);

            cryptStream = new CryptoStream(foutput, encrypt, CryptoStreamMode.Write);


            datafile = new byte[fs.Length];
            fs.Read(datafile, 0, datafile.Length);

            cryptStream.Write(datafile,0, datafile.Length);

            cryptStream.Close();
            foutput.Close();
            
            

        }

        public  void Decrypt(string input, string output)
        {

            StringBuilder fileExt = new StringBuilder();

            finput = new FileStream(input, FileMode.Open);
            
            
            fileExt.Append(output).Append(".decrypted");

            foutput = new FileStream(fileExt.ToString(), FileMode.Create);
           

            //Uncrypt
            ICryptoTransform ICrypt = algorythm.CreateDecryptor(data, data);

            cryptStream = new CryptoStream(foutput, ICrypt,  CryptoStreamMode.Write);

            byte[] filedata = new byte[finput.Length];

            finput.Read(filedata, 0, filedata.Length);


            cryptStream.Write(filedata, 0, filedata.Length);


            cryptStream.Close();
            foutput.Close();
            finput.Close();
           
            


        }

        public byte[] LoadDecryptedToMemory(string fname)
        {
            FileStream fstream = null;
            byte[] aux;
            if( File.Exists(fname)){

            fstream = new FileStream(fname, FileMode.Open);
            aux = new byte[fstream.Length];

            fstream.Read(aux, 0, aux.Length);
            fstream.Close();
            return aux;
            }

            return null;
        }



    }
}
