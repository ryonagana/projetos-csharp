﻿namespace IRCBot
{
    partial class conectaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gpConectar = new System.Windows.Forms.GroupBox();
            this.btCancelConnect = new System.Windows.Forms.Button();
            this.btSalvarConnect = new System.Windows.Forms.Button();
            this.lbCanal = new System.Windows.Forms.Label();
            this.lbNick = new System.Windows.Forms.Label();
            this.lbPort = new System.Windows.Forms.Label();
            this.lbServidor = new System.Windows.Forms.Label();
            this.cpCanal = new System.Windows.Forms.TextBox();
            this.cpNick = new System.Windows.Forms.TextBox();
            this.cpPort = new System.Windows.Forms.TextBox();
            this.cpServer = new System.Windows.Forms.TextBox();
            this.notificaErro = new System.Windows.Forms.ErrorProvider(this.components);
            this.gpConectar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.notificaErro)).BeginInit();
            this.SuspendLayout();
            // 
            // gpConectar
            // 
            this.gpConectar.Controls.Add(this.btCancelConnect);
            this.gpConectar.Controls.Add(this.btSalvarConnect);
            this.gpConectar.Controls.Add(this.lbCanal);
            this.gpConectar.Controls.Add(this.lbNick);
            this.gpConectar.Controls.Add(this.lbPort);
            this.gpConectar.Controls.Add(this.lbServidor);
            this.gpConectar.Controls.Add(this.cpCanal);
            this.gpConectar.Controls.Add(this.cpNick);
            this.gpConectar.Controls.Add(this.cpPort);
            this.gpConectar.Controls.Add(this.cpServer);
            this.gpConectar.Location = new System.Drawing.Point(12, 12);
            this.gpConectar.Name = "gpConectar";
            this.gpConectar.Size = new System.Drawing.Size(306, 163);
            this.gpConectar.TabIndex = 4;
            this.gpConectar.TabStop = false;
            this.gpConectar.Text = "Conectar";
            // 
            // btCancelConnect
            // 
            this.btCancelConnect.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btCancelConnect.Location = new System.Drawing.Point(183, 136);
            this.btCancelConnect.Name = "btCancelConnect";
            this.btCancelConnect.Size = new System.Drawing.Size(75, 23);
            this.btCancelConnect.TabIndex = 13;
            this.btCancelConnect.Text = "Cancelar";
            this.btCancelConnect.UseVisualStyleBackColor = true;
            // 
            // btSalvarConnect
            // 
            this.btSalvarConnect.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btSalvarConnect.Location = new System.Drawing.Point(102, 136);
            this.btSalvarConnect.Name = "btSalvarConnect";
            this.btSalvarConnect.Size = new System.Drawing.Size(75, 23);
            this.btSalvarConnect.TabIndex = 12;
            this.btSalvarConnect.Text = "Salvar";
            this.btSalvarConnect.UseVisualStyleBackColor = true;
            this.btSalvarConnect.Click += new System.EventHandler(this.btSalvarConnect_Click);
            // 
            // lbCanal
            // 
            this.lbCanal.AutoSize = true;
            this.lbCanal.Location = new System.Drawing.Point(6, 95);
            this.lbCanal.Name = "lbCanal";
            this.lbCanal.Size = new System.Drawing.Size(37, 13);
            this.lbCanal.TabIndex = 11;
            this.lbCanal.Text = "Canal:";
            // 
            // lbNick
            // 
            this.lbNick.AutoSize = true;
            this.lbNick.Location = new System.Drawing.Point(6, 56);
            this.lbNick.Name = "lbNick";
            this.lbNick.Size = new System.Drawing.Size(32, 13);
            this.lbNick.TabIndex = 10;
            this.lbNick.Text = "Nick:";
            // 
            // lbPort
            // 
            this.lbPort.AutoSize = true;
            this.lbPort.Location = new System.Drawing.Point(172, 16);
            this.lbPort.Name = "lbPort";
            this.lbPort.Size = new System.Drawing.Size(35, 13);
            this.lbPort.TabIndex = 9;
            this.lbPort.Text = "Porta:";
            // 
            // lbServidor
            // 
            this.lbServidor.AutoSize = true;
            this.lbServidor.Location = new System.Drawing.Point(6, 16);
            this.lbServidor.Name = "lbServidor";
            this.lbServidor.Size = new System.Drawing.Size(49, 13);
            this.lbServidor.TabIndex = 8;
            this.lbServidor.Text = "Servidor:";
            // 
            // cpCanal
            // 
            this.cpCanal.Location = new System.Drawing.Point(0, 110);
            this.cpCanal.Name = "cpCanal";
            this.cpCanal.Size = new System.Drawing.Size(159, 20);
            this.cpCanal.TabIndex = 7;
            this.cpCanal.Validated += new System.EventHandler(this.cpCanal_Validated);
            this.cpCanal.Validating += new System.ComponentModel.CancelEventHandler(this.cpCanal_Validating);
            // 
            // cpNick
            // 
            this.cpNick.Location = new System.Drawing.Point(0, 72);
            this.cpNick.Name = "cpNick";
            this.cpNick.Size = new System.Drawing.Size(159, 20);
            this.cpNick.TabIndex = 6;
            this.cpNick.Validated += new System.EventHandler(this.cpNick_Validated);
            this.cpNick.Validating += new System.ComponentModel.CancelEventHandler(this.cpNick_Validating);
            // 
            // cpPort
            // 
            this.cpPort.Location = new System.Drawing.Point(175, 32);
            this.cpPort.Name = "cpPort";
            this.cpPort.Size = new System.Drawing.Size(86, 20);
            this.cpPort.TabIndex = 5;
            this.cpPort.Validated += new System.EventHandler(this.cpPort_Validated);
            this.cpPort.Validating += new System.ComponentModel.CancelEventHandler(this.cpPort_Validating);
            // 
            // cpServer
            // 
            this.cpServer.Location = new System.Drawing.Point(0, 32);
            this.cpServer.Name = "cpServer";
            this.cpServer.Size = new System.Drawing.Size(159, 20);
            this.cpServer.TabIndex = 4;
            this.cpServer.Validated += new System.EventHandler(this.cpServer_Validated);
            this.cpServer.Validating += new System.ComponentModel.CancelEventHandler(this.cpServer_Validating);
            // 
            // notificaErro
            // 
            this.notificaErro.BlinkRate = 400;
            this.notificaErro.ContainerControl = this;
            // 
            // conectaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(329, 187);
            this.ControlBox = false;
            this.Controls.Add(this.gpConectar);
            this.Location = new System.Drawing.Point(337, 216);
            this.MaximumSize = new System.Drawing.Size(337, 216);
            this.Name = "conectaForm";
            this.ShowIcon = false;
            this.Text = "Conectar:";
            this.Load += new System.EventHandler(this.conectaForm_Load);
            this.gpConectar.ResumeLayout(false);
            this.gpConectar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.notificaErro)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gpConectar;
        private System.Windows.Forms.Label lbServidor;
        private System.Windows.Forms.TextBox cpCanal;
        private System.Windows.Forms.TextBox cpNick;
        private System.Windows.Forms.TextBox cpPort;
        private System.Windows.Forms.TextBox cpServer;
        private System.Windows.Forms.Button btSalvarConnect;
        private System.Windows.Forms.Label lbCanal;
        private System.Windows.Forms.Label lbNick;
        private System.Windows.Forms.Label lbPort;
        private System.Windows.Forms.Button btCancelConnect;
        private System.Windows.Forms.ErrorProvider notificaErro;

    }
}