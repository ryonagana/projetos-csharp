﻿namespace IRCBot
{
    partial class mainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.mMenu = new System.Windows.Forms.MenuStrip();
            this.arquivoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salvarLogToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.conectarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.desconectarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sairToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.barraStatus = new System.Windows.Forms.StatusStrip();
            this.barraLoading = new System.Windows.Forms.ToolStripProgressBar();
            this.statusBOT = new System.Windows.Forms.ToolStripStatusLabel();
            this.ttDicas = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lbCCanal = new System.Windows.Forms.Label();
            this.lbCNick = new System.Windows.Forms.Label();
            this.lbCPort = new System.Windows.Forms.Label();
            this.lbCServer = new System.Windows.Forms.Label();
            this.lbInfoCanal = new System.Windows.Forms.Label();
            this.lbInfoNick = new System.Windows.Forms.Label();
            this.lbInfoPort = new System.Windows.Forms.Label();
            this.lbInfoServer = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.pnPainel1 = new System.Windows.Forms.Panel();
            this.cMainText = new System.Windows.Forms.RichTextBox();
            this.lbConnect = new System.Windows.Forms.LinkLabel();
            this.gpUsers = new System.Windows.Forms.GroupBox();
            this.liUsers = new System.Windows.Forms.ListBox();
            this.mMenu.SuspendLayout();
            this.barraStatus.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.pnPainel1.SuspendLayout();
            this.gpUsers.SuspendLayout();
            this.SuspendLayout();
            // 
            // mMenu
            // 
            this.mMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.arquivoToolStripMenuItem});
            this.mMenu.Location = new System.Drawing.Point(10, 0);
            this.mMenu.Name = "mMenu";
            this.mMenu.Size = new System.Drawing.Size(542, 24);
            this.mMenu.TabIndex = 0;
            // 
            // arquivoToolStripMenuItem
            // 
            this.arquivoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.salvarLogToolStripMenuItem,
            this.conectarToolStripMenuItem,
            this.desconectarToolStripMenuItem,
            this.sairToolStripMenuItem});
            this.arquivoToolStripMenuItem.Name = "arquivoToolStripMenuItem";
            this.arquivoToolStripMenuItem.Size = new System.Drawing.Size(56, 20);
            this.arquivoToolStripMenuItem.Text = "Arquivo";
            // 
            // salvarLogToolStripMenuItem
            // 
            this.salvarLogToolStripMenuItem.Name = "salvarLogToolStripMenuItem";
            this.salvarLogToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.salvarLogToolStripMenuItem.Text = "Salvar Log";
            // 
            // conectarToolStripMenuItem
            // 
            this.conectarToolStripMenuItem.Name = "conectarToolStripMenuItem";
            this.conectarToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F5)));
            this.conectarToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.conectarToolStripMenuItem.Text = "Conectar";
            this.conectarToolStripMenuItem.Click += new System.EventHandler(this.conectarToolStripMenuItem_Click);
            // 
            // desconectarToolStripMenuItem
            // 
            this.desconectarToolStripMenuItem.Name = "desconectarToolStripMenuItem";
            this.desconectarToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F12)));
            this.desconectarToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.desconectarToolStripMenuItem.Text = "Desconectar";
            // 
            // sairToolStripMenuItem
            // 
            this.sairToolStripMenuItem.Name = "sairToolStripMenuItem";
            this.sairToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
            this.sairToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.sairToolStripMenuItem.Text = "Sair";
            // 
            // barraStatus
            // 
            this.barraStatus.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.barraLoading,
            this.statusBOT});
            this.barraStatus.Location = new System.Drawing.Point(10, 358);
            this.barraStatus.Name = "barraStatus";
            this.barraStatus.Size = new System.Drawing.Size(542, 22);
            this.barraStatus.TabIndex = 3;
            this.barraStatus.Text = "statusStrip1";
            // 
            // barraLoading
            // 
            this.barraLoading.Name = "barraLoading";
            this.barraLoading.Size = new System.Drawing.Size(100, 16);
            // 
            // statusBOT
            // 
            this.statusBOT.Name = "statusBOT";
            this.statusBOT.Size = new System.Drawing.Size(75, 17);
            this.statusBOT.Text = "Desconectado";
            // 
            // ttDicas
            // 
            this.ttDicas.IsBalloon = true;
            this.ttDicas.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.ttDicas.ToolTipTitle = "Aviso";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lbCCanal);
            this.groupBox1.Controls.Add(this.lbCNick);
            this.groupBox1.Controls.Add(this.lbCPort);
            this.groupBox1.Controls.Add(this.lbCServer);
            this.groupBox1.Controls.Add(this.lbInfoCanal);
            this.groupBox1.Controls.Add(this.lbInfoNick);
            this.groupBox1.Controls.Add(this.lbInfoPort);
            this.groupBox1.Controls.Add(this.lbInfoServer);
            this.groupBox1.Location = new System.Drawing.Point(380, 27);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(160, 135);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Info";
            // 
            // lbCCanal
            // 
            this.lbCCanal.AutoSize = true;
            this.lbCCanal.Location = new System.Drawing.Point(61, 111);
            this.lbCCanal.Name = "lbCCanal";
            this.lbCCanal.Size = new System.Drawing.Size(59, 13);
            this.lbCCanal.TabIndex = 15;
            this.lbCCanal.Text = "<Nenhum>";
            // 
            // lbCNick
            // 
            this.lbCNick.AutoSize = true;
            this.lbCNick.Location = new System.Drawing.Point(61, 80);
            this.lbCNick.Name = "lbCNick";
            this.lbCNick.Size = new System.Drawing.Size(59, 13);
            this.lbCNick.TabIndex = 14;
            this.lbCNick.Text = "<Nenhum>";
            // 
            // lbCPort
            // 
            this.lbCPort.AutoSize = true;
            this.lbCPort.Location = new System.Drawing.Point(61, 48);
            this.lbCPort.Name = "lbCPort";
            this.lbCPort.Size = new System.Drawing.Size(59, 13);
            this.lbCPort.TabIndex = 13;
            this.lbCPort.Text = "<Nenhum>";
            // 
            // lbCServer
            // 
            this.lbCServer.AutoSize = true;
            this.lbCServer.Location = new System.Drawing.Point(61, 20);
            this.lbCServer.Name = "lbCServer";
            this.lbCServer.Size = new System.Drawing.Size(59, 13);
            this.lbCServer.TabIndex = 12;
            this.lbCServer.Text = "<Nenhum>";
            // 
            // lbInfoCanal
            // 
            this.lbInfoCanal.AutoSize = true;
            this.lbInfoCanal.Location = new System.Drawing.Point(6, 111);
            this.lbInfoCanal.Name = "lbInfoCanal";
            this.lbInfoCanal.Size = new System.Drawing.Size(37, 13);
            this.lbInfoCanal.TabIndex = 11;
            this.lbInfoCanal.Text = "Canal:";
            // 
            // lbInfoNick
            // 
            this.lbInfoNick.AutoSize = true;
            this.lbInfoNick.Location = new System.Drawing.Point(6, 80);
            this.lbInfoNick.Name = "lbInfoNick";
            this.lbInfoNick.Size = new System.Drawing.Size(29, 13);
            this.lbInfoNick.TabIndex = 10;
            this.lbInfoNick.Text = "Nick";
            // 
            // lbInfoPort
            // 
            this.lbInfoPort.AutoSize = true;
            this.lbInfoPort.Location = new System.Drawing.Point(6, 50);
            this.lbInfoPort.Name = "lbInfoPort";
            this.lbInfoPort.Size = new System.Drawing.Size(35, 13);
            this.lbInfoPort.TabIndex = 9;
            this.lbInfoPort.Text = "Porta:";
            // 
            // lbInfoServer
            // 
            this.lbInfoServer.AutoSize = true;
            this.lbInfoServer.Location = new System.Drawing.Point(6, 20);
            this.lbInfoServer.Name = "lbInfoServer";
            this.lbInfoServer.Size = new System.Drawing.Size(49, 13);
            this.lbInfoServer.TabIndex = 8;
            this.lbInfoServer.Text = "Servidor:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.pnPainel1);
            this.groupBox2.Controls.Add(this.lbConnect);
            this.groupBox2.Location = new System.Drawing.Point(12, 27);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(362, 318);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Servidor:";
            // 
            // pnPainel1
            // 
            this.pnPainel1.AutoSize = true;
            this.pnPainel1.Controls.Add(this.cMainText);
            this.pnPainel1.Location = new System.Drawing.Point(3, 41);
            this.pnPainel1.Name = "pnPainel1";
            this.pnPainel1.Size = new System.Drawing.Size(359, 270);
            this.pnPainel1.TabIndex = 4;
            // 
            // cMainText
            // 
            this.cMainText.Location = new System.Drawing.Point(3, 10);
            this.cMainText.Name = "cMainText";
            this.cMainText.ReadOnly = true;
            this.cMainText.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
            this.cMainText.Size = new System.Drawing.Size(349, 257);
            this.cMainText.TabIndex = 0;
            this.cMainText.Text = "";
            // 
            // lbConnect
            // 
            this.lbConnect.AutoSize = true;
            this.lbConnect.LinkColor = System.Drawing.Color.SteelBlue;
            this.lbConnect.Location = new System.Drawing.Point(6, 22);
            this.lbConnect.Name = "lbConnect";
            this.lbConnect.Size = new System.Drawing.Size(50, 13);
            this.lbConnect.TabIndex = 3;
            this.lbConnect.TabStop = true;
            this.lbConnect.Text = "Conectar";
            this.lbConnect.VisitedLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.lbConnect.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lbConnect_LinkClicked_1);
            // 
            // gpUsers
            // 
            this.gpUsers.Controls.Add(this.liUsers);
            this.gpUsers.Location = new System.Drawing.Point(380, 178);
            this.gpUsers.Name = "gpUsers";
            this.gpUsers.Size = new System.Drawing.Size(160, 167);
            this.gpUsers.TabIndex = 6;
            this.gpUsers.TabStop = false;
            this.gpUsers.Text = "Usuarios";
            // 
            // liUsers
            // 
            this.liUsers.FormattingEnabled = true;
            this.liUsers.Location = new System.Drawing.Point(9, 19);
            this.liUsers.Name = "liUsers";
            this.liUsers.Size = new System.Drawing.Size(145, 134);
            this.liUsers.TabIndex = 0;
            // 
            // mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(552, 380);
            this.Controls.Add(this.gpUsers);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.barraStatus);
            this.Controls.Add(this.mMenu);
            this.MainMenuStrip = this.mMenu;
            this.MaximumSize = new System.Drawing.Size(560, 409);
            this.MinimumSize = new System.Drawing.Size(560, 409);
            this.Name = "mainForm";
            this.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.Text = "IRCBOT Ver 1.2";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.mainForm_KeyDown);
            this.mMenu.ResumeLayout(false);
            this.mMenu.PerformLayout();
            this.barraStatus.ResumeLayout(false);
            this.barraStatus.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.pnPainel1.ResumeLayout(false);
            this.gpUsers.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip mMenu;
        private System.Windows.Forms.ToolStripMenuItem arquivoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salvarLogToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem conectarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem desconectarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sairToolStripMenuItem;
        private System.Windows.Forms.StatusStrip barraStatus;
        private System.Windows.Forms.ToolStripProgressBar barraLoading;
        private System.Windows.Forms.ToolStripStatusLabel statusBOT;
        private System.Windows.Forms.ToolTip ttDicas;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lbInfoCanal;
        private System.Windows.Forms.Label lbInfoNick;
        private System.Windows.Forms.Label lbInfoPort;
        private System.Windows.Forms.Label lbInfoServer;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Panel pnPainel1;
        private System.Windows.Forms.RichTextBox cMainText;
        private System.Windows.Forms.LinkLabel lbConnect;
        private System.Windows.Forms.Label lbCNick;
        private System.Windows.Forms.Label lbCPort;
        private System.Windows.Forms.Label lbCServer;
        private System.Windows.Forms.Label lbCCanal;
        private System.Windows.Forms.GroupBox gpUsers;
        private System.Windows.Forms.ListBox liUsers;
    }
}

