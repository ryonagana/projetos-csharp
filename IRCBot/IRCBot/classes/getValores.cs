﻿using System;
using System.Collections.Generic;

using System.Text;

namespace IRCBot
{
   public class getValores
    {

        private string nServer;
        private string nPort;
        private string nNick;
        private string nChan;


        // Public Server;

        public string Servidor
        {

            get
            {
                Servidor = nServer;
                return nServer;

            }
            set
            {

                nServer = value;


            }
        }

        // Public Porta

        public string Porta
        {

            get
            {

                Porta = nPort;
                return nPort;
            }

            set
            {

                nPort = value;

            }

        }

        // Public Nick;

        public string Nickname
        {

            get
            {
                Nickname = nNick;
                return nNick;

            }
            set
            {

                nNick = value;

            }

        }

        //public Canal

        public string Canal
        {

            get
            {
                Canal = nChan;
                return nChan;

            }
            set
            {
                nChan = value;
            }

        }

        public string getServer()
        {

            return nServer;

        }

        public string getPort()
        {

            return nPort; 
        }

        public string getNick()
        {
            return nNick;

        }

        public string getCanal()
        {

            return nChan;
        }

    }
}