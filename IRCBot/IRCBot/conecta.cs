﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace IRCBot
{
    public partial class conectaForm : Form
    {

        public getValores Info = new getValores();
        

        public conectaForm()
        {
            InitializeComponent();


        }

        #region ValidaForm

        private bool ValidarForm()
        {


            if (cpServer.Text.Length == 0)
            {
                //btSalvarConnect.Enabled = false;
                return false;


            }
            else
            {

                if (cpPort.Text.Length == 0)
                {
                    //btSalvarConnect.Enabled = false;
                    return false;
                }
                else
                {

                    if (cpNick.Text.Length == 0)
                    {
                        //btSalvarConnect.Enabled = false;
                        return false;

                    }
                    else
                    {

                        if (cpCanal.Text.Length == 0)
                        {
                            //btSalvarConnect.Enabled = false;
                            return false;


                        }
                        else
                        {

                            return true;

                        }
                    }
                }


            }




    
            return false;
        }

        #endregion

        private void btSalvarConnect_Click(object sender, EventArgs e)
        {
            DialogResult dr = new DialogResult();



            if (!ValidarForm())
            {
                MessageBox.Show(this, "Por Favor preencha Corretamente os campos Acima", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.DialogResult = dr;


            }

                XmlTextWriter config = new XmlTextWriter("config.xml", Encoding.UTF8);

                try
                {
                    config.Formatting = Formatting.Indented;
                    config.Indentation = 6;
                    config.Namespaces = false;

                    config.WriteStartDocument();
                    config.WriteStartElement("config");

                    config.WriteStartElement("server");
                    config.WriteString(cpServer.Text);
                    config.WriteEndElement();

                    config.WriteStartElement("port");
                    config.WriteString(cpPort.Text);
                    config.WriteEndElement();

                    config.WriteStartElement("nick");
                    config.WriteString(cpNick.Text);
                    config.WriteEndElement();

                    config.WriteStartElement("canal");
                    config.WriteString(cpCanal.Text);
                    config.WriteEndElement();

                    config.WriteEndElement();
                    config.Flush();



                }
                catch (Exception ex)
                {

                    MessageBox.Show(this, "Erro nao pode ser criada o arquivo", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Stop);

                }
                finally
                {

                    if (config != null)
                    {
                        config.Close();

                    }

                }
   
  
      

        }

        private void conectaForm_Load(object sender, EventArgs e)
        {

        }

        private void cpServer_Validating(object sender, CancelEventArgs e)

        {
            if (cpServer.Text.Length == 0)
            {
                notificaErro.SetError(cpServer, "Campo Servidor deve estar preenchido");
                

            }
        }

        private void cpPort_Validating(object sender, CancelEventArgs e)
        {
            if (cpPort.Text.Length == 0)
            {
                notificaErro.SetError(cpPort, "Campo Porta deve estar preenchido");
           

            }
        }

        private void cpNick_Validating(object sender, CancelEventArgs e)
        {
            if (cpNick.Text.Length == 0)
            {
                notificaErro.SetError(cpNick, "Campo Nick deve estar preenchido");
                
             

            }
        }

        private void cpCanal_Validating(object sender, CancelEventArgs e)
        {
            if (cpCanal.Text.Length == 0)
            {
                notificaErro.SetError(cpCanal, "Campo Canal deve estar preenchido");
              
                

            }
        }

        private void cpServer_Validated(object sender, EventArgs e)
        {
            Info.Servidor = cpServer.Text;
        }

        private void cpPort_Validated(object sender, EventArgs e)
        {
            Info.Porta = cpPort.Text;
        }

        private void cpNick_Validated(object sender, EventArgs e)
        {
            Info.Nickname = cpNick.Text;
        }

        private void cpCanal_Validated(object sender, EventArgs e)
        {
            Info.Canal = cpCanal.Text;
        }

      
    }
}
