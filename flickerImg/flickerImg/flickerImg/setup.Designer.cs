﻿namespace flickerImg
{
    partial class setup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lbPorcDebug = new System.Windows.Forms.Label();
            this.btDeleteCache = new System.Windows.Forms.Button();
            this.btGetPhotos = new System.Windows.Forms.Button();
            this.btCancel = new System.Windows.Forms.Button();
            this.btOk = new System.Windows.Forms.Button();
            this.cpKeyword = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.statusbar = new System.Windows.Forms.StatusStrip();
            this.pgStatusbar = new System.Windows.Forms.ToolStripProgressBar();
            this.tsStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.groupBox1.SuspendLayout();
            this.statusbar.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lbPorcDebug);
            this.groupBox1.Controls.Add(this.btDeleteCache);
            this.groupBox1.Controls.Add(this.btGetPhotos);
            this.groupBox1.Controls.Add(this.btCancel);
            this.groupBox1.Controls.Add(this.btOk);
            this.groupBox1.Controls.Add(this.cpKeyword);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(403, 87);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Config";
            // 
            // lbPorcDebug
            // 
            this.lbPorcDebug.AutoSize = true;
            this.lbPorcDebug.Location = new System.Drawing.Point(306, 16);
            this.lbPorcDebug.Name = "lbPorcDebug";
            this.lbPorcDebug.Size = new System.Drawing.Size(73, 13);
            this.lbPorcDebug.TabIndex = 6;
            this.lbPorcDebug.Text = "Porcentagem:";
            this.lbPorcDebug.Visible = false;
            // 
            // btDeleteCache
            // 
            this.btDeleteCache.Location = new System.Drawing.Point(116, 58);
            this.btDeleteCache.Name = "btDeleteCache";
            this.btDeleteCache.Size = new System.Drawing.Size(101, 23);
            this.btDeleteCache.TabIndex = 5;
            this.btDeleteCache.Text = "Delete Cache";
            this.btDeleteCache.UseVisualStyleBackColor = true;
            this.btDeleteCache.Click += new System.EventHandler(this.btDeleteCache_Click);
            // 
            // btGetPhotos
            // 
            this.btGetPhotos.Location = new System.Drawing.Point(9, 58);
            this.btGetPhotos.Name = "btGetPhotos";
            this.btGetPhotos.Size = new System.Drawing.Size(101, 23);
            this.btGetPhotos.TabIndex = 4;
            this.btGetPhotos.Text = "Get New Photos";
            this.btGetPhotos.UseVisualStyleBackColor = true;
            this.btGetPhotos.Click += new System.EventHandler(this.btGetPhotos_Click);
            // 
            // btCancel
            // 
            this.btCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btCancel.Location = new System.Drawing.Point(322, 58);
            this.btCancel.Name = "btCancel";
            this.btCancel.Size = new System.Drawing.Size(75, 23);
            this.btCancel.TabIndex = 3;
            this.btCancel.Text = "Close";
            this.btCancel.UseVisualStyleBackColor = true;
            this.btCancel.Click += new System.EventHandler(this.btCancel_Click);
            // 
            // btOk
            // 
            this.btOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btOk.Location = new System.Drawing.Point(241, 58);
            this.btOk.Name = "btOk";
            this.btOk.Size = new System.Drawing.Size(75, 23);
            this.btOk.TabIndex = 2;
            this.btOk.Text = "OK";
            this.btOk.UseVisualStyleBackColor = true;
            // 
            // cpKeyword
            // 
            this.cpKeyword.Location = new System.Drawing.Point(9, 32);
            this.cpKeyword.Name = "cpKeyword";
            this.cpKeyword.Size = new System.Drawing.Size(388, 20);
            this.cpKeyword.TabIndex = 1;
            this.cpKeyword.Text = "Forest, mountains, trees";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(156, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Keyword: (separe with commas)";
            // 
            // statusbar
            // 
            this.statusbar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pgStatusbar,
            this.tsStatus});
            this.statusbar.Location = new System.Drawing.Point(0, 115);
            this.statusbar.Name = "statusbar";
            this.statusbar.Size = new System.Drawing.Size(427, 22);
            this.statusbar.TabIndex = 1;
            this.statusbar.Text = "statusStrip1";
            // 
            // pgStatusbar
            // 
            this.pgStatusbar.Name = "pgStatusbar";
            this.pgStatusbar.Size = new System.Drawing.Size(100, 16);
            this.pgStatusbar.Step = 1;
            // 
            // tsStatus
            // 
            this.tsStatus.Name = "tsStatus";
            this.tsStatus.Size = new System.Drawing.Size(0, 17);
            // 
            // setup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(427, 137);
            this.Controls.Add(this.statusbar);
            this.Controls.Add(this.groupBox1);
            this.Name = "setup";
            this.Text = "Config ScreenSaver";
            this.Load += new System.EventHandler(this.setup_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.statusbar.ResumeLayout(false);
            this.statusbar.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox cpKeyword;
        public System.Windows.Forms.Button btCancel;
        public System.Windows.Forms.Button btOk;
        private System.Windows.Forms.Button btDeleteCache;
        private System.Windows.Forms.Button btGetPhotos;
        private System.Windows.Forms.StatusStrip statusbar;
        private System.Windows.Forms.ToolStripProgressBar pgStatusbar;
        private System.Windows.Forms.Label lbPorcDebug;
        private System.Windows.Forms.ToolStripStatusLabel tsStatus;
    }
}