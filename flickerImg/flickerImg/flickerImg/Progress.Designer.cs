﻿namespace flickerImg
{
    partial class Progress
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gpProgress = new System.Windows.Forms.GroupBox();
            this.cpStatus = new System.Windows.Forms.Label();
            this.pgBar = new System.Windows.Forms.ProgressBar();
            this.btCancel = new System.Windows.Forms.Button();
            this.gpProgress.SuspendLayout();
            this.SuspendLayout();
            // 
            // gpProgress
            // 
            this.gpProgress.Controls.Add(this.cpStatus);
            this.gpProgress.Controls.Add(this.pgBar);
            this.gpProgress.Controls.Add(this.btCancel);
            this.gpProgress.Location = new System.Drawing.Point(12, 2);
            this.gpProgress.Name = "gpProgress";
            this.gpProgress.Size = new System.Drawing.Size(288, 84);
            this.gpProgress.TabIndex = 0;
            this.gpProgress.TabStop = false;
            this.gpProgress.Text = "Progress";
            // 
            // cpStatus
            // 
            this.cpStatus.AutoSize = true;
            this.cpStatus.Location = new System.Drawing.Point(17, 55);
            this.cpStatus.Name = "cpStatus";
            this.cpStatus.Size = new System.Drawing.Size(0, 13);
            this.cpStatus.TabIndex = 2;
            // 
            // pgBar
            // 
            this.pgBar.Location = new System.Drawing.Point(6, 19);
            this.pgBar.Name = "pgBar";
            this.pgBar.Size = new System.Drawing.Size(276, 23);
            this.pgBar.TabIndex = 1;
            // 
            // btCancel
            // 
            this.btCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btCancel.Location = new System.Drawing.Point(207, 55);
            this.btCancel.Name = "btCancel";
            this.btCancel.Size = new System.Drawing.Size(75, 23);
            this.btCancel.TabIndex = 0;
            this.btCancel.Text = "Cancel";
            this.btCancel.UseVisualStyleBackColor = true;
            this.btCancel.Click += new System.EventHandler(this.btCancel_Click);
            // 
            // Progress
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(309, 98);
            this.Controls.Add(this.gpProgress);
            this.Name = "Progress";
            this.Text = "Progress";
            this.gpProgress.ResumeLayout(false);
            this.gpProgress.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gpProgress;
        public System.Windows.Forms.Button btCancel;
        public System.Windows.Forms.Label cpStatus;
        public System.Windows.Forms.ProgressBar pgBar;
    }
}