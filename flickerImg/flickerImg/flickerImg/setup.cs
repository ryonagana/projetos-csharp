﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FlickrNet;
using System.IO;
using System.Xml;
namespace flickerImg
{
    public partial class setup : Form
    {
        BackgroundWorker backgroundWorker = new BackgroundWorker();
        BackgroundWorker deleteWork = new BackgroundWorker();

        Progress pg = new Progress();
        Config conf = new Config();
        

        FlickrNet.Flickr Client = new FlickrNet.Flickr("9b5ac85c9e5ab1724ee237e47004ae42", "51f230928c330e10");
        flickerImg.PhotosList List;
        FlickrNet.PhotoSearchOptions Options = new FlickrNet.PhotoSearchOptions();
        string[] strFiles;

        private void DeleteCache()
        {
            string[] filelist = Directory.GetFiles(Global.AbsolutePath, "*.*");

            foreach (string name in filelist)
            {

                File.Delete(Global.AbsolutePath + @"/" + name);


            }

           
            

        }

        public setup()
        {
            InitializeComponent();

            if (!Directory.Exists(System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "//.flickrimgdata"))
            {
                Directory.CreateDirectory(System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "//.flickrimgdata");
            }

            strFiles = Directory.GetFiles(System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "//.flickrimgdata//", "*.jpg");

           
            Options.Licenses.Add(LicenseType.AttributionNoncommercialCC);
            Options.Licenses.Add(LicenseType.NoKnownCopyrightRestrictions);
            Options.Licenses.Add(LicenseType.AttributionShareAlikeCC);
            Options.PerPage = 10;
            Options.Page = 12;
            Options.Tags = "mountains, valley";
            Options.Accuracy = GeoAccuracy.World;

            pg.pgBar.Minimum = 0;
            pg.pgBar.Maximum = 100;

            pgStatusbar.Maximum = 100;
            pgStatusbar.Minimum = 0;


            List = new PhotosList();

            btGetPhotos.Tag = (int)0;


            backgroundWorker.WorkerReportsProgress = true;
            backgroundWorker.WorkerSupportsCancellation = true;


            backgroundWorker.DoWork += new DoWorkEventHandler(backgroundWorker_DoWork);
            backgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(backgroundWorker_RunWorkerCompleted);
            backgroundWorker.ProgressChanged += new ProgressChangedEventHandler(backgroundWorker_ProgressChanged);

            deleteWork.WorkerReportsProgress = true;
            deleteWork.WorkerSupportsCancellation = false;

            deleteWork.DoWork += new DoWorkEventHandler(deleteWork_DoWork);
            deleteWork.ProgressChanged += new ProgressChangedEventHandler(deleteWork_ProgressChanged);
            deleteWork.RunWorkerCompleted += new RunWorkerCompletedEventHandler(deleteWork_RunWorkerCompleted);

            try
            {
                if (File.Exists(System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "//.flickrimgdata//config.xml"))
                {

                    XmlReader tr = XmlReader.Create(System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "/.flickrimgdata/config.xml"); 

                    cpKeyword.Text = tr.GetAttribute("xmlns")[0].ToString();
                }
            }
            catch (Exception e)
            {
            }
           
            
        }

        void deleteWork_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            btGetPhotos.Enabled = true;
            btOk.Enabled = true;
            btCancel.Enabled = true;
          
            tsStatus.Text = "All Files Sucessfully deleted!";
            
        }

        void deleteWork_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
           
            pgStatusbar.Increment(e.ProgressPercentage);
            
        }

        void deleteWork_DoWork(object sender, DoWorkEventArgs e)
        {
            string[] filelist = Directory.GetFiles(Global.AbsolutePath, "*.*");
            int num = 0;
            foreach (string name in filelist)
            {

                File.Delete(name);
                num++;
            }


            deleteWork.ReportProgress(num * 2);

        }

        void backgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {


            pgStatusbar.Increment(e.ProgressPercentage);
            lbPorcDebug.Text = "Porcentagem: " + e.ProgressPercentage;
        }

        void backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //throw new NotImplementedException();
            btDeleteCache.Enabled = true;
            btOk.Enabled = true;
            btCancel.Enabled = true;

            btGetPhotos.Tag = (int)0;
            btGetPhotos.Text = "Get New Photos";
            tsStatus.Text = "Completed!";

           

        }

        void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            

            BackgroundWorker bw = sender as BackgroundWorker;
            int num = 0;
            

                FlickrNet.PhotoCollection photoArray = Client.PhotosSearch(Options);

                try
                {

                    if (strFiles.Length < 10)
                    {
                        conf.Init();
                        conf.AddTag("config");

                        conf.AddTag("keyword");
                        conf.WriteElementParent("searchkey", "keyword", cpKeyword.Text);
                        conf.CloseTag();


                        conf.AddTag("images");

                        foreach (FlickrNet.Photo photo in photoArray)
                        {

                            List.Add(photo.PhotoId, new PhotoContainer(photo));
                            List.GetURL(photo.PhotoId);
                            tsStatus.Text = "Downloading " + photo.PhotoId + ".jpg ...";
                            num++;
                            backgroundWorker.ReportProgress(num * 2);



                            conf.AddTag("photo");

                            conf.WriteElementParent("background", "url", photo.LargeUrl);
                            conf.WriteElementParent("background", "id", photo.PhotoId);
                            conf.WriteElementParent("background", "absolutepath", Global.AbsolutePath + photo.PhotoId + ".jpg");


                            conf.CloseTag();
                            tsStatus.Text = "Writing on " + photo.PhotoId + ".jpg ..." + "on config";




                        }

                        conf.CloseTag();
                        conf.CloseTag();
                        conf.EndDocument();

                    }
                }
                catch (Exception Error)
                {
                    MessageBox.Show(Error.Message);

                }









               
            //throw new NotImplementedException();
        }

        private void btGetPhotos_Click(object sender, EventArgs e)
        {
            Options.Tags = cpKeyword.Text;
            string[] total = Directory.GetFiles(Global.AbsolutePath, "*.jpg");

            if (total.Length == 10)
            {
                if (MessageBox.Show("You've generated images already!\n if you want new pictures click on \"Delete Cache\" and then \"Get New Photos\"\n Do You Want to Clear Cache Now and Reload? (All files will be Deleted! and Downloaded again) ", "Warning!", MessageBoxButtons.OKCancel) == System.Windows.Forms.DialogResult.OK)
                {
                    deleteWork.RunWorkerAsync();
                    List.Clear();
                    backgroundWorker.RunWorkerAsync();
                    return;
                }
                else
                {
                    return;
                }

            }


            if ((int)btGetPhotos.Tag == 0)
            {

                backgroundWorker.RunWorkerAsync();
                btGetPhotos.Tag = (int)1;
                btGetPhotos.Text = "Cancel";

            }else  if ((int)btGetPhotos.Tag == 1)
            {

                backgroundWorker.CancelAsync();

            }

            if (backgroundWorker.IsBusy)
            {
                btDeleteCache.Enabled = false;
                btOk.Enabled = false;
                btCancel.Enabled = false;
            }

          
               
            

            
           

             
        }

        private void btCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btDeleteCache_Click(object sender, EventArgs e)
        {

            btGetPhotos.Enabled = false;
            btOk.Enabled = false;
            btCancel.Enabled = false;
            List.Clear();
            deleteWork.RunWorkerAsync();

        }

        private void setup_Load(object sender, EventArgs e)
        {

        }
    }
}
