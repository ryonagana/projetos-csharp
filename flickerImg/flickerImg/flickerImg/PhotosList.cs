﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Drawing;

namespace flickerImg
{
   public class PhotosList : Dictionary<string, PhotoContainer>
    {

       public PhotosList()
       {

       }


       
       

        public void GetURL(string uri)
        {
            HttpWebRequest req = (HttpWebRequest) WebRequest.Create(this[uri].URL);
            HttpWebResponse response = (HttpWebResponse) req.GetResponse();

            Stream stream = response.GetResponseStream();

            Image img = Image.FromStream(stream);

            img.Save(System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "//.flickrimgdata//" + uri + ".jpg");
            stream.Close();
            
            

        }
      

    }
}
