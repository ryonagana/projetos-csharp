﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace teslacoil.Versions
{
    static class Version
    {
        public  const string ProgramName = "cbIRCBot";
        public  const int VersionMajor =    0;
        public  const int VersionMinor =    2;
        public  const int VersionBuild =    0;
        public  const int VersionRevision = 1;

        public  static StringBuilder VersionString = new StringBuilder(ProgramName + " " + VersionMajor + "."  + VersionMinor + "." + VersionBuild  + "." + VersionRevision);


    }
}
