﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

namespace teslacoil
{
    class ThemeButton : Control
    {
        int State;

        Color color1 = Color.FromArgb(31, 31, 31);
        Color color2 = Color.FromArgb(41, 41, 41);
        Color color3 = Color.FromArgb(51, 51, 51);
        Color color4 = Color.FromArgb(0, 0, 0);
        Color color5 = Color.FromArgb(25, 255, 255, 255);

        protected override void OnMouseEnter(EventArgs e)
        {
            State = 1;
            Invalidate();
            base.OnMouseEnter(e);
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            State = 2;
            Invalidate();
            base.OnMouseDown(e);
        }


        protected override void OnMouseLeave(EventArgs e)
        {
            State = 0;
            Invalidate();
            base.OnMouseLeave(e);
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            State = 1;
            Invalidate();
            base.OnMouseUp(e);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            using (Bitmap bmp = new Bitmap(Width, Height))
            {
                using (Graphics gp = Graphics.FromImage(bmp))
                {
                    gp.DrawRectangle(new Pen(color1), 0, 0, Width - 1, Height - 1);

                    if (State == 2)
                    {
                        Drawings.Draw.Gradient(gp, color2, color3, 1, 1, Width - 2, Height - 2);
                    }
                    else
                    {
                        Drawings.Draw.Gradient(gp, color3, color2, 1, 1, Width - 2, Height - 2);
                    }

                    SizeF measure = gp.MeasureString(Text, Font);
                    gp.DrawString(Text, Font, new SolidBrush(ForeColor), ((Width / 2) - measure.Width / 2), ((Height / 2) - measure.Height / 2));

                    Drawings.Draw.Blend(gp, color4, color5, color4, (float)0.5, 0, 1, 1, Width - 2, 1);

                    e.Graphics.DrawImage(bmp, 0, 0);

                }
            }

            base.OnPaint(e);
        }




    }
}
