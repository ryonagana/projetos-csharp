﻿namespace teslacoil
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cpStatus = new teslacoil.ThemeRichTextboxControl();
            this.btDesconectar = new teslacoil.ThemeButton();
            this.themeButton2 = new teslacoil.ThemeButton();
            this.btStart = new teslacoil.ThemeButton();
            this.theme1 = new teslacoil.Theme();
            this.SuspendLayout();
            // 
            // cpStatus
            // 
            this.cpStatus.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.cpStatus.Location = new System.Drawing.Point(12, 36);
            this.cpStatus.Name = "cpStatus";
            this.cpStatus.Size = new System.Drawing.Size(292, 209);
            this.cpStatus.TabIndex = 6;
            this.cpStatus.Text = "\n";
            // 
            // btDesconectar
            // 
            this.btDesconectar.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btDesconectar.Location = new System.Drawing.Point(171, 251);
            this.btDesconectar.Name = "btDesconectar";
            this.btDesconectar.Size = new System.Drawing.Size(133, 74);
            this.btDesconectar.TabIndex = 5;
            this.btDesconectar.Text = "Frangar (Desconectar)";
            this.btDesconectar.Click += new System.EventHandler(this.btDesconectar_Click);
            // 
            // themeButton2
            // 
            this.themeButton2.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.themeButton2.Location = new System.Drawing.Point(274, 0);
            this.themeButton2.Name = "themeButton2";
            this.themeButton2.Size = new System.Drawing.Size(42, 18);
            this.themeButton2.TabIndex = 4;
            this.themeButton2.Text = "Fechar";
            this.themeButton2.Click += new System.EventHandler(this.themeButton2_Click);
            // 
            // btStart
            // 
            this.btStart.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.btStart.Location = new System.Drawing.Point(12, 251);
            this.btStart.Name = "btStart";
            this.btStart.Size = new System.Drawing.Size(133, 74);
            this.btStart.TabIndex = 1;
            this.btStart.Text = "FINISH HIM!!!";
            this.btStart.Click += new System.EventHandler(this.btStart_Click);
            // 
            // theme1
            // 
            this.theme1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.theme1.Location = new System.Drawing.Point(0, 0);
            this.theme1.Name = "theme1";
            this.theme1.Size = new System.Drawing.Size(316, 338);
            this.theme1.TabIndex = 3;
            this.theme1.Text = "theme1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(316, 338);
            this.Controls.Add(this.cpStatus);
            this.Controls.Add(this.btDesconectar);
            this.Controls.Add(this.themeButton2);
            this.Controls.Add(this.btStart);
            this.Controls.Add(this.theme1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form1";
            this.Text = "Tesla Coil";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private ThemeButton btStart;
        private Theme theme1;
        private ThemeButton themeButton2;
        private ThemeButton btDesconectar;
        private ThemeRichTextboxControl cpStatus;
    }
}

