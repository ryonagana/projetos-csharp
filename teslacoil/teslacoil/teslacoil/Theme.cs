﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

namespace teslacoil
{

   

    

    class Theme : Control
    {
        Color color1 = Color.FromArgb(255, 255, 255);
        Color color2 = Color.FromArgb(63, 63, 63);
        Color color3 = Color.FromArgb(41, 41, 41);
        Color color4 = Color.FromArgb(27, 27, 27);
        Color color5 = Color.FromArgb(0,0, 0, 0);
        Color color6 = Color.FromArgb(25, 255, 255, 255);

        private int title_height = 25;

        private int TitleWidth
        {
            get
            {
                return title_height;
               
            }
            set
            {
                int v = 0;
                if (v > Height) v = 1;
                else if (v < Height) Height = 1;
                title_height = v;
                this.Invalidate();
            }
        }

       


       
        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                Point p = new Point();
                p = Control.MousePosition;
                p.X = p.X - (Width / 2);
                p.Y = p.Y - (Height / 2);

                this.Parent.Location = p;

            }

            this.Parent.Invalidate();
            
            
            //base.OnMouseDown(e);
        }


        protected override void OnHandleCreated(EventArgs e)
        {
            Dock = DockStyle.Fill;

            if (this.Parent.GetType() == typeof(Form))
            {
                
                Form parent = (Form) this.Parent;
                parent.FormBorderStyle = FormBorderStyle.None;

                this.Parent = parent;
            }

            base.OnHandleCreated(e);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            using (Bitmap bmp = new Bitmap(Width, Height))
            {
                using (Graphics gp = Graphics.FromImage(bmp))
                {
                    string title =  Versions.Version.VersionString.ToString();
                    
                    gp.Clear(color4);
                    Drawings.Draw.Gradient(gp, color2, color3, 1, 1, Width - 2, Height - 2);

                    Drawings.Draw.Gradient(gp, color4, color3, 0, 0, Width, title_height);
                    SizeF textMeasure = gp.MeasureString(this.Parent.Text, this.Parent.Font, Width / 2);

                    Rectangle rect = new Rectangle( (Width / 2) - (int)textMeasure.Width + 5 , ( (title_height + 2) / 2 - (int) textMeasure.Height /2)  , (int) Width, (int) textMeasure.Height);

                    using (LinearGradientBrush top = new LinearGradientBrush(rect, color1, color3, LinearGradientMode.Vertical))
                    {
                        gp.DrawString(this.Parent.Text, Font, top, rect);
                    }

                    gp.DrawLine(new Pen(color3), 0, 1, Width, 1);
                    
                    

                    e.Graphics.DrawImage((Image) bmp.Clone(), 0, 0);
                }

            }

           
            

           // base.OnPaint(e);
        }

        


       
        
    }
}
