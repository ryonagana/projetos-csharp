﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Forms;


namespace teslacoil
{
    [StructLayout(LayoutKind.Auto)]
    struct configdata
    {
        public string ip;
        public int port;
        public int delay;
        public int threads;

    }

    class ConfigKill : parseConfig
    {
        public configdata cfg = new configdata();

        public ConfigKill(string path) : base(path)
        {

            if (!File.Exists(FileIni))
            {
                FileStream fStream = new FileStream(FileIni, FileMode.Create);
                StreamWriter sw = new StreamWriter(fStream);

                sw.WriteLine("[config]");
                sw.WriteLine("ip={0}", "127.0.0.1");
                sw.WriteLine("port={0}", "80");
                sw.WriteLine("espera={0}", "10");
                sw.WriteLine("processos={0}", "1");


                sw.Flush();
                sw.Close();
                MessageBox.Show("Nao Existe Arquivo de configuração entao foi gerado\n\n por favor reabra o programa", "Aviso:", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                Application.Exit();
            }
            else
            {
                cfg.ip = getIni("config", "ip");
                cfg.port =  Int32.Parse(getIni("config", "port"));
                cfg.delay = Int32.Parse(getIni("config", "espera"));
                cfg.threads = Int32.Parse(getIni("config", "processos"));
                

            }



        }

    }
}
