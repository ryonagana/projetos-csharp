﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.InteropServices;
using Nini;
using Nini.Ini;
using Nini.Util;


namespace teslacoil
{
    public class parseConfig
    {

        IniDocument readIni = null;


        public string FileIni { get; set; }

        /// <summary>

        /// Construtor de Ini.

        /// </summary>

        /// <PARAM name="path"></PARAM>


        public parseConfig(string path)
        {
            if (path.Length > 0)
            {
                FileIni = Path.GetFullPath(path);
            }


            try
            {
                readIni = new IniDocument(FileIni);
            }
            catch (FileNotFoundException fex)
            {



            }
        }

        /// <summary>

        /// Essa Função pega o valor de um INI desde que ja exista o arquivo

        /// </summary>

        /// <PARAM name="section"></PARAM>
        /// <PARAM name="key"></PARAM>

        public string getIni(string section, string key)
        {


            return readIni.Sections[section].GetValue(key);
        }

        public void writeIni(string section, string key, string value)
        {
            readIni.Sections[section].Set(key, value);
            readIni.Save(FileIni);

        }

        public void CreateSection(string section)
        {
            readIni.Sections.Add(new IniSection(section));
            readIni.Save(FileIni);

        }

        public void SaveIni()
        {
            readIni.Save(FileIni);

        }


    }
}
