﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

namespace bot.Drawings
{
    class Draw
    {
        public static void Gradient(Graphics g, Color c1, Color c2, int x, int y, int width, int height)
        {
            Rectangle rect = new Rectangle(x, y, width, height);

            using (LinearGradientBrush gradient = new LinearGradientBrush(rect, c1, c2, LinearGradientMode.Vertical))
            {
                g.FillRectangle(gradient, rect);

            }

        }

        public static void Blend(Graphics g, Color c1, Color c2, Color c3, float c, int d, int x, int y, int width, int height)
        {

            ColorBlend blend = new ColorBlend(3);
            blend.Colors = new Color[3] { c1, c2, c3 };
            blend.Positions = new float[3] { 0, c, 1 };

            Rectangle rect = new Rectangle(x, y, width, height);

            using (LinearGradientBrush brush = new LinearGradientBrush(rect, c1, c1, (LinearGradientMode)d))
            {
                brush.InterpolationColors = blend;
                g.FillRectangle(brush, rect);

            }



        }


    }
}
