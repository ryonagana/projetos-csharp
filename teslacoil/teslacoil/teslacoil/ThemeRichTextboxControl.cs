﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

namespace teslacoil
{
    class ThemeRichTextboxControl : Control
    {
        Color color1 = Color.FromArgb(31, 31, 31);
        Color color2 = Color.FromArgb(41, 41, 41);
        Color color3 = Color.FromArgb(51, 51, 51);
        Color color4 = Color.FromArgb(0, 0, 0);
        Color color5 = Color.FromArgb(25, 255, 255, 255);

        public RichTextBox rtBox = new RichTextBox();

        public ThemeRichTextboxControl()
        {
            this.Text = "\n";
            

            rtBox.Size = new Size(Width, Height);
            rtBox.Location = new Point(0, 0);
            this.Controls.Add(rtBox);
            Invalidate();

            rtBox.TextChanged += new EventHandler(rtBox_TextChanged);


        }

        void rtBox_TextChanged(object sender, EventArgs e)
        {
            Invalidate();
            // throw new NotImplementedException();
        }


        protected override void OnTextChanged(EventArgs e)
        {
            Invalidate();
            base.OnTextChanged(e);
        }
        protected override void OnKeyUp(KeyEventArgs e)
        {
            Invalidate();
            base.OnKeyUp(e);
        }

        protected override void OnKeyPress(KeyPressEventArgs e)
        {
            Invalidate();
            base.OnKeyPress(e);
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            Invalidate();
            base.OnKeyDown(e);
        }

        

        protected override void OnPaint(PaintEventArgs e)
        {
            using (Bitmap bmp = new Bitmap(Width, Height))
            {
                using (Graphics gp = Graphics.FromImage(bmp))
                {
                    
                    Drawings.Draw.Gradient(gp, color2, color3, 1, 1, Width - 2, Height - 2);

                    SizeF measure = gp.MeasureString(Text, Font);
                    gp.DrawString(Text, Font, new SolidBrush(Color.Black), 3, 3);
                    gp.DrawString(Text, Font, new SolidBrush(ForeColor), 3, 1);
                    

                    Drawings.Draw.Blend(gp, color4, color5, color4, (float)0.5, 0, 1, 1, Width - 2, 1);


                    e.Graphics.DrawImage(bmp, 0, 0);
                }

            }

           
            base.OnPaint(e);
        }

    }
}
